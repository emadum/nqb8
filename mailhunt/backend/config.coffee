_ = require 'lodash'
config = require './config/base.json'
env = if process.env.NODE_ENV? then process.env.NODE_ENV else 'development'
envConfig = require "./config/#{env}.json"
merged = _.merge config, envConfig
merged.env = env
module.exports = merged
