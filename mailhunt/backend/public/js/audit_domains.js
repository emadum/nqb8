$(function() {
    function initBackgrid(){
        Backgrid.InputCellEditor.prototype.attributes.class = 'form-control input-sm';

        var Domain = Backbone.Model.extend({
            initialize: function () {
                Backbone.Model.prototype.initialize.apply(this, arguments);
                this.on("change", function (model, options) {
                if (options && options.save === false) return;
                  model.save();
                });
            },
            sync: function (method, model, options) {
                console.log(method, model, options);
                if (method == 'create') options.url = '/admin/domains';
                return Backbone.sync(method, model, options);
            }
        });
        var filterNew = false;
        var filters = {};
        var runAllFilters = function(el) {
            return _.reduce(_.keys(filters), function(memo, filter) {
                return memo && filters[filter](el);
            }, true);
        };
        var filterTable = function() {
            if (!_.keys(filters).length) {
                createBackgrid(pageableDomains);
                return;
            }
            filteredCollection = initialDomains.fullCollection.filter(runAllFilters);
            createBackgrid(new PageableDomains(filteredCollection, {
                state: {
                    firstPage: 1,
                    currentPage: 1
                }
            }));
        };

        var dataUrl = "/stats/page/trending";

        var PageableDomains = Backbone.PageableCollection.extend({
            model: Domain,
            url: function () {
                return dataUrl;
            },
            state: {
                pageSize: 13
                //sortKey: 'hits',
                //order: -1
            },
            mode: "client" // page entirely on the client side
        });

        var HumanReadableDateCell = Backgrid.DatetimeCell.extend({
            formatter: {
                fromRaw: function(rawData, model) {
                    parts = moment(rawData).format("MMM DD, YYYY h:mm a").split(" ");
                    amPm = parts.pop().substr(0,1);
                    return parts.join(" ") + amPm;
                },
                toRaw: function(formattedData, model) {
                    return moment(formattedData);
                }
            }
        });
        var ImageCell = Backgrid.Cell.extend({
            formatter: {
                fromRaw: function(rawData, model) {
                    return rawData;
                },
                toRaw: function(formattedData, model) {
                    return formattedData;
                }
            },
            render: function () {
                this.$el.empty();
                var rawValue = this.model.get(this.column.get("name"));
                var formattedValue = this.formatter.fromRaw(rawValue, this.model);
                if (rawValue && rawValue.length)
                    this.$el.append($("<img>", {class: "company-logo", src: rawValue}));
                this.delegateEvents();
                return this;
            }
        });
        var DomainLinkCell = Backgrid.Cell.extend({
            formatter: {
                fromRaw: function(rawData, model) {
                    return "http://" + rawData.split('.').slice(-2).join('.');
                },
                toRaw: function(formattedData, model) {
                    return rawData.split('//').pop();
                },
            }, 
            render: function () {
                this.$el.empty();
                var rawValue = this.model.get(this.column.get("name"));
                var formattedValue = this.formatter.fromRaw(rawValue, this.model);
                this.$el.append(
                    $("<a>", {
                      tabIndex: -1,
                      target: '_blank',
                      href: formattedValue
                    }).text(rawValue), 
                    " ",
                    $("<a>", {
                      style: "float: right",
                      tabIndex: -1,
                      href: (adminMode ? "/admin" : "") + "/domains/" + rawValue,
                      target: '_blank',
                      class: "fa fa-area-chart"
                    })
                );
                this.delegateEvents();
                return this;
            }
        
        });

        var pageableDomains = new PageableDomains(),
            initialDomains = pageableDomains;

        // Page Filter
        $("#page-filter label").on("click", function() {
            page = $.trim($(this).text());
            dataUrl = "/stats/page/" + page;
            $("#domain-export-csv").attr("href", dataUrl)
            pageableDomains.fetch({
                reset: true,
                success: function() {
                    filterTable();
                }
            });
        });
        function createBackgrid(collection){
            var columns = [
            {
                name: "organization",
                label: "Name",
                cell: "string",
                editable: true
            }, {
                name: "logo_url",
                label: "Logo",
                cell: Backgrid.Extension.CloudinaryImageCell,
                editable: true
            }, {
                name: "description",
                label: "Description",
                cell: Backgrid.Extension.TextCell,
                editable: true
            }, {
                name: "domain",
                label: "Domain",
                cell: DomainLinkCell,
                editable: false
            }];

            if (LightBlue.isScreen('xs')){
                columns.splice(3,1);
            }
            var pageableGrid = new Backgrid.Grid({
                columns: columns,
                collection: collection,
                className: 'table table-striped table-editable no-margin mb-sm'
            });
      if (pageableDomains.state.sortKey) {
    console.log(pageableDomains.state);
    pageableGrid.sort(pageableDomains.state.sortKey, pageableDomains.state.order == 1 ? "descending" : "ascending");
      }

            var paginator = new Backgrid.Extension.Paginator({

                windowSize: 20,

                slideScale: 0.25, // Default is 0.5

                // Whether sorting should go back to the first page
                goBackFirstOnSort: false, // Default is true

                collection: collection,

                controls: {
                    rewind: {
                        label: '<i class="fa fa-angle-double-left fa-lg"></i>',
                        title: "First"
                    },
                    back: {
                        label: '<i class="fa fa-angle-left fa-lg"></i>',
                        title: "Previous"
                    },
                    forward: {
                        label: '<i class="fa fa-angle-right fa-lg"></i>',
                        title: "Next"
                    },
                    fastForward: {
                        label: '<i class="fa fa-angle-double-right fa-lg"></i>',
                        title: "Last"
                    }
                }
            });

            $("#table-dynamic").html('').append(pageableGrid.render().$el).append(paginator.render().$el);
        }

        $("#search-domains").keyup(function(){
            var $that = $(this);
            filters.domain = function(el){
                return ~el.get('domain').toUpperCase().indexOf($that.val().toUpperCase());
            };
            filterTable();
        });

        PjaxApp.onResize(function(){createBackgrid(pageableDomains)});
        createBackgrid(pageableDomains);
        pageableDomains.fetch();
    }
    function pageLoad(){
        $('.widget').widgster();
        initBackgrid();
    }
    window.pageLoad = pageLoad;
    PjaxApp.onPageLoad(pageLoad);
    pageLoad();

});
