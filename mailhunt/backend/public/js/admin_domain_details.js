$(function() {
  $("#fullcontact-json").css("max-height", $("table").height()-20);
  var initChart = function(selector, data) {
    console.log("init chart", data);
    var chart = 
      nv.models.lineChart()
      .margin({left: 30, bottom: 40, right: 30})
      .color(keyColor);

    chart.yAxis
    .showMaxMin(false)
    .tickFormat(d3.format(',.f'));

    chart.xAxis
    .showMaxMin(false)
    .tickFormat(function(d) { return d3.time.format('%b %d')(moment().week(d).startOf('week').toDate()) });

    d3.select(selector)
    .datum(data)
    .transition().duration(500).call(chart);

    //PjaxApp.onResize(chart.update);
  }
  function pageLoad() {
    $.getJSON("/stats/domainCharts/" + currentDomain + "/2015", function(data) {
      initChart('#user-chart-line svg', [{
        key: "Total",
        values: _.map(data, function(item) {
          return {x: item.week, y: item.total_users};
        })
      }, {
        key: "Active",
        values: _.map(data, function(item) {
          return {x: item.week, y: item.active_users};
        })
      }]);
      initChart('#message-chart-line svg', [{
        key: "Messages",
        values: _.map(data, function(item) {
          return {x: item.week, y: item.messages};
        })
      }]);
    });
  }
  PjaxApp.onPageLoad(pageLoad);
  pageLoad();
});
