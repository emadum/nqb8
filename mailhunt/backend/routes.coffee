_ = require 'lodash'
async = require 'async'
useragent = require 'useragent'
moment = require 'moment'
gcloud = require 'gcloud'
mongoose = require 'mongoose'
path = require 'path'
Cacheman = require 'cacheman'
#cloudinary = require 'cloudinary'

GmailAuth = require './models/GmailAuth'
Domain = require './models/Domain'
Message = require './models/Message'
HistoryItem = require './models/HistoryItem'
WeekDomainCount = require './models/WeekDomainCount'
Company = require './models/Company'
User = require './models/User'
BatchImport = require './models/BatchImport'


Importer = require './lib/MailHunt/Importer'
BigQuery = require './lib/MailHunt/BigQuery'
Admin = require './lib/MailHunt/Admin'
Top100 = require './lib/MailHunt/Top100'

config = require './config'

#cloudinary.config config.cloudinary

cache = new Cacheman 'default',
  ttl: 90
  engine: 'redis'
  port: 6379
  host: '127.0.0.1'

cached = (key, expire, getValue, cb) ->
  try
    cache.get key, (err, value) ->
      throw err if err
      if value?
        console.log "Pulled #{key} from cache"
        cb value
      else
        console.log "Calculating #{key}"
        getValue (fresh) ->
          console.log "Storing #{key} result in cache"
          cache.set key, fresh, expire
          cb fresh
  catch err
    console.log "Error fetching `#{key}` from cache: #{err}"
    cb()

sortDomainStats = (result) -> _.sortBy result, (item) -> -item.newUsers

updateDomainInCache = (props) ->
  domain = props.domain
  async.each ['popular', 'new', 'trending']
  , (page, cb) ->
    try
      cache.get "#{page}_apps", (err, apps) ->
        return cb() if err?
        return cb() unless apps?
        found = false
        for app in apps
          continue unless app.domain is domain
          found = true
          console.log "Found #{domain} in #{page.toUpperCase()} - need to update"        
          console.log JSON.stringify props
          for key in ['organization', 'description']            
            app[key] = props[key] if props[key]?
          app.logo_url = props.logo if props.logo?
          cache.set "#{page}_apps", apps, (err) ->
            if err?
              console.log "Error updating values for #{domain} in cache: #{err}"
            else
              console.log "Updating values for #{domain} in #{page.toUpperCase()}"
            cb()
        cb() unless found
    catch err
      console.log "Error fetching `#{page}_apps` from cache: #{err}"
      cb()
  , (err) ->
    console.log "Error updating #{domain} in cache: #{err}" if err?

getMongoStats = (done) ->
  mongoose.connection.db.command
    dbStats: 1
    scale: 1024*1024
  , (err, result) ->
    done null, result

getDomainCount = (done) ->
  Domain.find().count (err, count) -> done null, count

getMessageCount = (done) ->
  Message.find().count (err, count) -> done null, count

getUsers = (done) ->
  GmailAuth.find().sort
    joined: 1
  .exec (err, result) ->
    async.map result, (user, cb) ->
      params =
        email: user.email
        valid: user.valid
        joined: user.joined
      HistoryItem.getUserHistory user.email, (history) ->          
        unless history
          params.last_import = 'Never'
        else            
          params.last_import = moment(history.last_import_finish).fromNow()
          params.total_domains = history.total_domains or 0
          params.total_messages = history.total_messages
          params.first_email_date = history.first_email[0]
          params.first_email_error = history.first_email[1]
          params.last_email_date = history.last_email          
          params.last_import_error = history.last_import_error
          params.duration = history.duration
        cb null, params
    , (err2, results) ->        
      done null, results

getBigQueryStats = (done) ->
  c = config.bigquery
  mhbq = new BigQuery c.project_id, c.dataset, path.resolve __dirname, c.key_file
  mhbq.getTableMetadata 'messages', (metadata) ->
    ret =
      fileSize: 0
      numRows: 0
    if metadata?
      ret.fileSize = metadata.numBytes/(1024*1024)
      ret.numRows = metadata.numRows
    done null, ret

loginUser = (req, res) ->
  gmailApi = new Importer config.googleapi  
  gmailApi.getAccessToken req.query.code, ->
    gmailApi.fetchUser (email) ->      
      unless email?
        console.log "Login attempt failed, fetchUser returned no email"
        return res.redirect '/'
      req.session.email = email
      switch req.session.on_auth_ok
        when 'import'
          gmailApi.monitorQuota()
          gmailApi.import {}, () ->
            console.log "Web triggered import done for #{gmailApi.emailAddress}"
          res.redirect '/'
         when 'waitList'
          if req.session.refcode?
            User.set email, {ref_code: req.session.refcode}, (err) ->
              console.log if err? then "Error saving refcode #{req.session.refcode}" else "Saved refcode #{req.session.refcode}"
          console.log "Showing waitlist"
          res.redirect '/waitlist'
        else
          if req.session.on_auth_ok?.indexOf '/admin' is 0
            loginAdmin req, res, email
          else
            res.redirect '/'

loginAdmin = (req, res, email) ->
  #console.log "Confirming admin user..."
  User.isAdmin email
  , () ->
    #console.log "Admin user #{email} confirmed"
    req.session.adminUser = email
    res.redirect req.session.on_auth_ok
  , () ->
    #console.log "User #{email} not an admin"
    res.status(301).end()

adminImportDetails = (req, res) ->
  BatchImport.findOne
    _id: req.params.id
  .exec (err, batchImport) ->
    return res.status 400 unless batchImport?
    HistoryItem.find
      import_start:
        $gte: batchImport.import_start
        $lte: batchImport.import_end
    .exec (err, items) ->
      for x in items
        x.duration = moment.duration(moment(x.import_finish).diff(moment x.import_start)).humanize()
      res.render 'admin_import_details',
        batch: batchImport
        duration: moment.duration(moment(batchImport.import_end).diff(moment batchImport.import_start)).humanize()
        items: items
        #section: 'Recent Imports'
        title: 'Import Details'
        admin_mode: true
        show_nav: true
        email: req.session.adminUser

module.exports =
  
  index: (req, res) ->
    #console.log "Session email: #{req.session.email}"
    unless req.session.email?
      res.render 'index',
        layout: 'landing'
    else
      res.render 'admin_domains',
        section: 'Domains'
        title: 'Product Domains'
        admin_mode: false
        show_nav: false
        email: req.session.email

#  import: (req, res) ->
#    gmailApi = new Importer config.googleapi
#    req.session.on_auth_ok = 'import'
#    res.redirect gmailApi.getAuthUrl()

  joinList: (req, res) ->
    gmailApi = new Importer config.googleapi
    req.session.on_auth_ok = 'waitList'
    if req.params.refcode?
      console.log "USER JOINED WITH REFCODE #{req.params.refcode}"
      req.session.refcode = req.params.refcode
    req.session.save (err) -> res.redirect gmailApi.getAuthUrl()

  waitList: (req, res) ->
    #if config.import.import_on_join
    GmailAuth.find().sort
      joined: 1
    .exec (err, result) ->
      position = _.findIndex result, (item) ->
        if item.email is req.session.email then true else false
      res.render 'waitList',
        layout: 'basic'
        title: 'ProductFans'
        position: 2835+position
        user: req.session.email
        joined: result[position]?.joined.getTime()

  trending: (req, res) ->
    cached 'trending_apps', '24h', Top100.trendingApps, (apps) ->
      res.render 'top100',
        layout: 'basic'
        title: 'ProductFans - Top 100 - Trending Apps'
        count_label: 'New Users'
        apps: apps

  popular: (req, res) ->
    cached 'popular_apps', '24h', Top100.popularApps, (apps) ->
      res.render 'top100',
        layout: 'basic'
        title: 'ProductFans - Top 100 - Popular Apps'
        count_label: 'Total Users'
        apps: apps

  new: (req, res) ->
    cached 'new_apps', '24h', Top100.newApps, (apps) ->
      res.render 'top100',
        layout: 'basic'
        title: 'ProductFans - Top 100 - New Apps'
        count_label: 'Total Users'
        apps: apps

  domainChartStats: (req, res) ->
    domain = req.params.domain
    year = req.params.year
    Domain.find
      domain: domain
    .exec (err, result) ->
      unless err? or not result.length
        subdomains = result[0].subdomains
        if subdomains?.length
          subdomains.push domain
          domain =
            $in: subdomains
      WeekDomainCount.getDomainUsers domain, year, (result) -> res.json result

  timeSpanDomainStats: (req, res) ->
    format = req.params.format
    timeSpan = req.params.timeSpan
    year = parseInt req.params.year
    length = parseInt req.params.length if req.params.length?
    exclude = req.query.exclude
    prepareData = unless exclude? then sortDomainStats else (result) ->
      sortDomainStats result
    ret = switch format
      when 'json'
        (result) -> res.json prepareData result
      when 'csv'
        outFile = _.compact([year, timeSpan, length]).join '-'
        (result) ->
          res.setHeader 'Content-disposition', "attachment; filename=#{outFile}.csv"
          res.csv prepareData result
    switch timeSpan
      when 'week' then WeekDomainCount.getWeekTotals year, length, exclude, ret
      when 'month' then WeekDomainCount.getMonthTotals year, length, exclude, ret
      when 'quarter' then WeekDomainCount.getQuarterTotals year, length, exclude, ret
      when 'year' then WeekDomainCount.getYearTotals year, exclude, ret

  pageStats: (req, res) ->
    page = req.params.page
    console.log "Getting stats for #{page}"
    cached "#{page}_apps", '24h', Top100["#{page}Apps"], (apps) -> res.json apps

  domainCompanyData: (req, res) ->
    key = config.fullcontact.api_key
    Company.getFullContact key, req.params.domain, (err, result) ->
      res.json result

  adminImports: (req, res) ->
    return adminImportDetails req, res if req.params.id?
    BatchImport.find().exec (err, result) ->
      res.render 'admin_imports',
        imports: result
        section: 'Recent Imports'
        title: 'Batch Import'
        admin_mode: true
        show_nav: true
        email: req.session.adminUser

  adminDomains: (req, res) ->
    res.render 'admin_domains',
      section: 'Domains'
      title: 'Product Domains'
      admin_mode: true
      show_nav: true
      email: req.session.adminUser
      switchTo: req.session.switchTo

  adminAuditDomains: (req, res) ->
    #cloudinary.cloudinary_js_config()
    res.render 'audit_domains',
      section: 'Domains'
      title: 'Audit'
      admin_mode: true
      show_nav: true
      email: req.session.adminUser

  adminDomainDetails: (req, res) ->
    params =
      section: 'Domain'
      title: req.params.domain
      admin_mode: if req.session.is_admin then true else false
      show_nav: if req.session.is_admin then true else false
      email: req.session.email
    Domain.get req.params.domain, (props) ->
      unless props?
        params.error = "Domain not found."
      else 
        params.subdomains = props.subdomains
        params.earliest = moment(props.earliest).format 'MMMM Do YYYY, h:mm:ss a'
        params.total = props.total_messages
        params.company_desc = props.description
        if props.company?
          params.company_name = props.company.name
          profile = _.find props.company.fullContact.response.socialProfiles, (item) -> item.bio?
          params.company_desc = profile.bio if profile?.bio?
          params.full_contact = props.company.fullContact
      res.render 'admin_domain_details', params

  adminUpdateDomain: (req, res) ->
    return res.sendStatus 400 unless req.body
    Domain.findOne
      domain: req.body.domain
    .exec (err, result) ->
      return res.sendStatus 400 if err? or not result?
      changed = false
      req.body.logo = req.body.logo_url if req.body.logo_url?
      #console.log JSON.stringify result.toObject()
      for key in ['organization', 'description', 'logo']
        continue unless req.body[key]?
        continue if result[key]? and req.body[key] is result[key]
        result[key] = req.body[key]
        #console.log "Changed #{key} to #{req.body[key]}"
        changed = true
      if changed
        result.override = true
        result.save (err) ->
          #console.log "SAVED (err=#{err})"
          #console.log JSON.stringify result.toObject()
          updateDomainInCache result.toObject()
          res.json
            success: if err? then false else true
            changed: true
      else 
        res.json
          success: true
          changed: false

  auth: (req, res) ->
    unless req.query.error?
      loginUser req, res
    else
      req.session.error = "Authorization failed: #{req.query.error}<br/><a href='/import'>Try again</a>"
      res.redirect '/'

  login: (req, res) ->
    req.session.is_admin = false
    gmailApi = new Importer config.googleapi
    res.redirect gmailApi.getAuthUrl()

  logout: (req, res) ->
    delete req.session.email if req.session.email?
    req.session.save (err) -> res.redirect '/'

  adminLogout: (req, res) ->
    delete req.session.adminUser if req.session.adminUser?
    req.session.is_admin = false
    req.session.save (err) -> res.redirect '/'

  adminSwitchUser: (req, res) ->    
    req.session.switchTo = if req.params.email is 'none' then false else req.params.email
    req.session.save (err) -> res.redirect '/admin/domains'

  adminUserDetails: (req, res) ->
    tmpl =
      section: 'User Details'
      title: req.params.email
      admin_mode: true
      show_nav: true
      email: req.session.adminUser
    GmailAuth.find
      email: req.params.email
    , (err, results) ->  
      tmpl.user = results[0] unless err? or not results.length
      WeekDomainCount.getUserDomains req.params.email, 2015, (domains) ->
        tmpl.domains = domains.slice 0, 10
        res.render 'admin_user', tmpl

  admin: (req, res) ->
    tmpl =
      title: "Admin Home"
      admin_mode: true
      show_nav: true
      email: req.session.adminUser
    queries = [getMongoStats, getMessageCount, getUsers, getBigQueryStats, getDomainCount]
    async.parallel queries, (err,results) ->
      tmpl.usage = results[0]
      tmpl.messageCount = results[1]
      tmpl.users = results[2]
      tmpl.userCount = results[2].length
      tmpl.bigquery = results[3]
      tmpl.domainCount = results[4]
      res.render 'admin', tmpl
