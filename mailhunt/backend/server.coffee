http = require 'http'
express = require 'express'
csv = require 'express-csv'
exphbs = require 'express-handlebars'
bodyParser = require 'body-parser'
Session = require 'express-session'
mongoose = require 'mongoose'
moment = require 'moment-timezone'
MongoStore = require('connect-mongo') Session
schedule = require 'node-schedule'

routes = require './routes'
config = require './config'
admin = require './lib/MailHunt/Admin'

console.log "Environment: #{config.env}"
console.log JSON.stringify config, null, 2

#passport = require 'passport'
#GooglePlusStrategy = require('passport-google-plus').Strategy

app = express()

tzm = (date) -> moment(date).tz config.locale.timezone

# Set handlebars as the templating engine
app.engine '.hbs', exphbs
  defaultLayout: 'main'
  extname: '.hbs'
  helpers:
    toFixed: (num) -> if num.toFixed? then num.toFixed 2 else num
    commas: (num) -> num.toString().replace /\B(?=(\d{3})+(?!\d))/g, ','
    joinCommas: (array) -> array.join ', '
    json: (obj) -> JSON.stringify obj, null, 2
    mdy: (obj) -> if obj? then tzm(obj).format "MMM Do YYYY" else '-'
    mdyHms: (obj) -> if obj? then tzm(obj).format "MMM Do YYYY H:mm:ss" else '-'
    hmsa: (obj) -> if obj? then tzm(obj).format "h:mm:ss a" else '-'
app.set 'view engine', '.hbs'

# Disable etag headers on responses to prevent browser caching
app.disable 'etag'

# Connect to MongoDB
mongoose.connect "mongodb://#{config.mongodb.host}/#{config.mongodb.database}", (err) ->
  console.log "Error connecting to Mongo: ", err if err?

# Sessions
session = Session
  resave: false
  saveUninitialized: false
  secret: "3fnl34ifghu3liugh"
  store: new MongoStore
    mongooseConnection: mongoose.connection
app.use session

# Nightly Imports
nightly = schedule.scheduleJob
  hour: 7
  minute: 15
, () ->
  console.log "Running scheduled import..."
  admin.import.update () ->
    console.log "Done running scheduled import!"
    # Analyze new users since last import
    # Run bigquery load

# Google Auth
# passport.use new GooglePlusStrategy
#   clientId: '1098310743763-1okt2a9nsmodh6na91cmbnaeeo5b6ddb.apps.googleusercontent.com'
#   clientSecret: 'O6H05SpCaAamYgEiGAMOIk90'
# , (identifier, profile, done) ->
#   console.log "PASSPORT LOGIN ***********"
#   console.log identifier
#   console.log profile
#   done null, profile, identifier

isAdmin = (req, res, next) ->
  console.log "Checking admin"
  unless req.session.adminUser?
    console.log "Trying to access admin route #{req.route.path}"
    req.session.on_auth_ok = req.route.path
    res.redirect '/login'
    return
  req.session.is_admin = true
  req.session.save()
  next()

##########
# Routes #
##########

# Public views
app.get '/', (req, res) -> res.sendFile "#{__dirname}/public/index.html"
app.get '/domains/:domain', routes.adminDomainDetails

# Gmail Data Import
# app.get '/import', routes.import

# Splash page
# Waitlist
app.get '/join/:refcode?', routes.joinList
app.get '/waitlist', routes.waitList
# Basic informational pages
basicPages =
  faq: "ProductFans - FAQ"
  #privacy: "ProductFans - Privacy Policy"
for page, title of basicPages
  app.get '/' + page, (req, res) ->
    res.render page,
      layout: 'basic'
      title: title

# Admin
app.get '/admin/logout', isAdmin, routes.adminLogout
app.get '/admin', isAdmin, routes.admin
app.get '/admin/imports/:id?', isAdmin, routes.adminImports
app.get '/admin/user/:email', isAdmin, routes.adminUserDetails
app.get '/admin/audit', isAdmin, routes.adminAuditDomains
app.get '/admin/domains', isAdmin, routes.adminDomains
app.post '/admin/domains', isAdmin, bodyParser.json(), routes.adminUpdateDomain
app.get '/admin/domains/:domain', isAdmin, routes.adminDomainDetails
app.get '/admin/switch/:email', isAdmin, routes.adminSwitchUser

# JSON and CSV data
app.get '/stats/domainCharts/:domain/:year', routes.domainChartStats
app.get '/stats/page/:page', routes.pageStats
app.get '/stats/:format/domain/:timeSpan/:year/:length?', routes.timeSpanDomainStats
app.get '/company/:domain', routes.domainCompanyData

# Auth
app.get '/auth', routes.auth
app.get '/login', routes.login
app.get '/logout', routes.logout

# Top 100
app.get '/trending', routes.trending
app.get '/popular', routes.popular
app.get '/new', routes.new

#app.post '/auth/google/callback', passport.authenticate 'google', (req, res) -> res.send req.user

# Set /public as static content dir
app.use '/', express.static __dirname + '/public/'

# Jacked up and good to go
server = http.createServer app
port = process.env.PORT or 6666
server.listen port, -> console.log 'Express server listening on port ' + port

# var ios = require('socket.io-express-session');
# var io = require('socket.io')(server);
# io.use(ios(session)); // session support 
# io.on('connection', function(socket){
#   console.log(socket.handshake.session);
# });