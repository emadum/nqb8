# NQB8 -- MailHunt
# Author: Eric Adum (eric@nqb8.com)

# 3rd Party Modules
moment = require 'moment'
_ = require 'lodash'
async = require 'async'
google = require 'googleapis'

# Models
Thread = require '../models/Thread'
GmailAuth = require '../models/GmailAuth'

OAuth2Client = google.auth.OAuth2
gmail = google.gmail 'v1'

module.exports =
class GmailAPI
  captureHeaders: ['delivered-to', 'from', 'to', 'date', 'list-id']
  concurrency: 5

  constructor: (config) ->
    c = config
    @forceApproval = c.force_approval
    @oauth2Client = new OAuth2Client c.client_id, c.client_secret, c.redirect_url

  import: () ->
    @totalEntries = 0
    @threadsProcessed = 0
    @importStart = new Date()
    @cancel = {}
    @mapFetchId = 0
    @waitingForList = false
    @error = false
    @monitorQuota()

  monitorQuota: () ->
    @queuedCalls = []
    @totalQuota = 0
    @quotaPerSecond = 0
    @idle = 0
    @timeouts = 0
    @consecutiveTimeouts = 0
    console.log "Monitoring quota..."
    @rateMonitorInterval = setInterval (() => @quotaMonitor()), 1000

  quotaMonitor: () ->
    console.log "=====> [#{@emailAddress}] Quota usage this second: #{@quotaPerSecond} <====="
    if @quotaPerSecond
      @idle = 0
      @consecutiveTimeouts = 0
    else
      ++@idle
    @quotaPerSecond = 0
    if @queuedCalls.length
      console.log "Executing #{@queuedCalls.length} queued calls from last second..."
      @queuedCalls.pop()() while @queuedCalls.length
    else if @idle >= 10
      console.log "Timed out (x#{@timeouts}) after 10 seconds, re-fetching messages"
      ++@timeouts
      if ++@consecutiveTimeouts > 3
        console.log "#{@consecutiveTimeouts} timeouts in a row"
        @error = 'Timed out'
        return @finished()
      if @currentMapFetch?
        @cancel[@currentMapFetch] = true
        @currentMapFetch = null
        console.log "Cancelling mapFetch #{@currentMapFetch}"
      @fetchMessages @savedParams
      @idle = 0

  quotaUsed: (amount) ->
    @quotaPerSecond += amount
    @totalQuota += amount

  loadSavedAuth: (email, cb) ->
    GmailAuth.find
      email: email
    , (err, result) =>
      return cb err if err?
      return cb "No saved auth for #{email}" unless result.length
      @emailAddress = email
      @refreshAccess result[0], cb

  refreshAccess: (saved, cb) ->
    @oauth2Client.setCredentials
        access_token: saved.access_token
        refresh_token: saved.refresh_token
    @oauth2Client.refreshAccessToken (err, tokens) =>
      if err?
        console.log "Auth error: #{err}"
        saved.error =
          message: err
          date: new Date()
        saved.valid = false
        saved.save (err) ->
          console.log "Saved error to mongo"
        return cb err
      return "No tokens" unless tokens?
      @oauth2Client.setCredentials tokens
      updated = _.assign saved, tokens
      updated.save (err) ->
        console.log "Error saving updated creds to mongo" if err
      cb null, true

  getAuthUrl: ->
    params =
      access_type: 'offline'
      scope: 'https://www.googleapis.com/auth/gmail.readonly'
    params.approval_prompt = 'force' if @forceApproval
    return @oauth2Client.generateAuthUrl params
    
  getAccessToken: (code, cb) ->
    # request access token
    @oauth2Client.getToken code, (err, tokens) =>
      # set tokens to the client
      # TODO: tokens should be set by OAuth2 client.
      console.log tokens
      if tokens?
        if "expiry_date" in tokens
          console.log "Access expires #{moment(tokens.expiry_date).fromNow()}"
        @authTokens = tokens
        @oauth2Client.setCredentials tokens
        cb true
      else
        cb false
      return
    return

  fetchUser: (cb) ->
    #console.log "Hitting API: gmail.users.getProfile"
    queryParams =
      userId: 'me'
      auth: @oauth2Client
    gmail.users.getProfile queryParams, (err, results) =>
      if err
        console.log 'An error occured (fetchUser)', err
        return @finished()
      @quotaUsed 1
      @emailAddress = results.emailAddress
      if @authTokens?
        params = _.assign @authTokens,
          email: @emailAddress
        delete @authTokens.token_type
        console.log "Saving auth", JSON.stringify params, null, 2
        GmailAuth.save params, (insertOK) ->
          console.log "Failed to save auth token to Mongo" unless insertOK  
      cb @emailAddress

  applyParams: (params) ->
    queryParams =
      userId: @emailAddress
      auth: @oauth2Client
      q: ""
    if params?
      @savedParams = params
      queryParams = _.assign queryParams, params
    queryParams.pageToken = @nextPageToken if @nextPageToken?
    return queryParams

  fetchThreads: (params) ->
    console.log "fetchThreads called in #{@mode} mode"
    #console.log "Hitting API: gmail.users.threads.list"
    queryParams = @applyParams params
    gmail.users.threads.list queryParams, (err, results) =>
      if err
        console.log 'An error occured (fetchThreads) on page #{@nextPageToken}', err        
        return @finished()
      @quotaUsed 10
      if results and results.threads
        #console.log results
        console.log "------------------------------------------------------"
        console.log "[#{@emailAddress}] QUERY => #{queryParams.q}"
        console.log "Fetched #{results.threads.length} threads on page #{@nextPageToken}"
        console.log "Next page token: #{results.nextPageToken}"
        console.log "Estimated total results: #{results.resultSizeEstimate}"
        console.log "------------------------------------------------------"
        @threads = results.threads
        @nextPageToken = results.nextPageToken
        @handleNextThread()
      else
        console.log "No results for query" + JSON.stringify queryParams, null, 2
        @finished()

  handleNextThread: ->
    if @threads.length
      # Check if we already have this thread in Mongo
      nextThread = @threads.shift()
      ++@threadsProcessed
      Thread.checkForNewMessages @emailAddress, nextThread, (result, thread) =>
        if result is true
          console.log "Thread doesn't exist or was updated: #{nextThread.id}"
          @fetchThread nextThread, 3
        else
          console.log "No new messages in thread #{nextThread.id} (mode=#{@mode})"
          @handleUnchangedThread thread
    else
      console.log "Done, next page? #{@nextPageToken}"
      if @nextPageToken?
        if @savedParams? then @fetchThreads @savedParams else @fetchThreads()
      else
        @finished()

  handleUnchangedThread: (thread) -> @handleNextThread()

  finished: ->
    clearInterval @rateMonitorInterval
    console.log "Total quota used: #{@totalQuota}"
    delete @savedParams
                  
  fetchThread: (thread, retry) ->
    #console.log 'Fetching thread ' + thread.id 
    #console.log "Hitting API: gmail.users.threads.get"
    queryParams =
      userId: @emailAddress
      id: thread.id
      auth: @oauth2Client
      format: 'metadata'
    gmail.users.threads.get queryParams, (err, result) =>
      if err
        console.log 'An error occured (fetchThread)', err
        console.log "Retries remaining: #{retry}"
        if retry > 0
          @fetchThread thread, retry-1
        else
          @finished()
        return
      @quotaUsed 10
      #console.log "#{result.messages.length} messages in thread #{thread.id}"
      threadInfo =
        user: @emailAddress
        thread_id: thread.id
        thread_count: result.messages.length
        history_id: thread.historyId
      @processThread threadInfo, thread.id, result.messages
      @handleNextThread()

  fetchMessage: (msgId, cb) ->
    if @quotaPerSecond >= 150
      #console.log "Quota this second hit #{@quotaPerSecond}, queuing call for next second"
      @queuedCalls.push () => @fetchMessage msgId, cb
    else
      gmail.users.messages.get
        userId: @emailAddress
        auth: @oauth2Client
        id: msgId
      , (err, result) =>
        @quotaUsed 5
        cb err, result

  processThread: (threadInfo, threadId, messages) -> console.log "Process thread"

  fetchMessages: (params) ->
    queryParams = @applyParams params
    console.log "---> fetchMessages [#{@emailAddress}] QUERY => #{queryParams.q} <---"
    if @waitingForList
      return console.log "Still waiting for previous fetchMessages query to return"
    @waitingForList = true
    gmail.users.messages.list queryParams, (err, results) =>
      @waitingForList = false
      if err
        console.log 'An error occured (fetchMessages) on page #{@nextPageToken}', err
        @finished()
        return
      @quotaUsed 5
      if results and results.messages
        console.log "------------- fetchMessages response -----------------"
        console.log "Fetched #{results.messages.length} messages on page #{@nextPageToken}"
        console.log "Next page token: #{results.nextPageToken}"
        console.log "Estimated total results: #{results.resultSizeEstimate}"
        console.log "------------------------------------------------------"
        @mapFetchMessages results.messages, (fetched) =>
          console.log "Finished processing. Total messages: #{fetched.length}"
          @processMessages fetched, (err) =>
            @nextPageToken = results.nextPageToken
            if @nextPageToken? then @fetchMessages @savedParams else @finished()
      else
        console.log "No results for query" + JSON.stringify queryParams, null, 2
        @finished()

  queuedFetchMessages: (messageList, drain) ->
    console.log "queueFetch #{messageList.length} message"
    messages = []
    q = async.queue (message, cb) => @fetchMessage message.id, (err, result) ->
      messages.push result unless err?
      cb err
    , @concurrency
    q.drain = () -> drain messages
    q.push messageList

  mapFetchMessages: (messageList, done) ->
    console.log "mapFetch #{messageList.length} message"
    uuid = ++@mapFetchId
    @currentMapFetch = uuid
    async.mapLimit messageList, @concurrency
    , (message, cb) => 
      if @cancel[uuid]
        cb "mapFetch #{uuid} cancelled"
      else
        @fetchMessage message.id, cb
    , (fetchErr, messages) ->
      if fetchErr?
        console.log "Error fetching messages: #{fetchErr}"
      else
        console.log "mapFetch #{uuid} finished"
      done messages

  processMessages: (messages, done) ->
    console.log "Process Messages:" + JSON.stringify messages, null, 2
    done()

  fetchEmail: (messageId) ->
    #console.log 'Fetching message ' + messageId
    #console.log "Hitting API: gmail.users.messages.get"
    gmail.users.messages.get {
      userId: @emailAddress
      id: messageId
      auth: @oauth2Client
      format: 'metadata'
    }, (err, result) ->
      if err
        console.log 'An error occured', err
        return
      @quotaUsed 5
      console.log JSON.stringify result, null, 4

  updateDateRange: (date) ->
    if @latestMessage?      
      @latestMessage = date if date.isAfter @latestMessage
    else
      @latestMessage = date
    if @earliestMessage?
      @earliestMessage = date if date.isBefore @earliestMessage
    else
      @earliestMessage = date