GmailAPI = require '../GmailApi'
HistoryItem = require '../../models/HistoryItem'
Message = require '../../models/Message'
Domain = require '../../models/Domain'
WeekDomainCount = require '../../models/WeekDomainCount'
User = require '../../models/User'

Stats = require './Stats'

fs = require 'fs'
_ = require 'lodash'
async = require 'async'
moment = require 'moment'

parse_domain = require 'parse-domain'
parseDomain = _.memoize (domain) ->
  return domain unless domain?
  parts = parse_domain domain.split(" ")[0]
  unless parts?
    console.log "Unable to parse domain #{domain}"
    domain
  else
    parts.domain + '.' + parts.tld

stripName = (person) ->
  start = person.indexOf '<'
  return person.toLowerCase() if start is -1
  email = person.substr start + 1
  return (email.slice 0, -1).toLowerCase()

getHeader = (headers, field) ->
  _.result _.find(headers, (header) ->
    header.name.toLowerCase() is field.toLowerCase()), 'value'

module.exports =
class MailHuntImporter extends GmailAPI

  categories: ['promotions','social','updates','purchases','finances']

  constructor: (config) ->
    super config
    @fetchMessages = _.throttle @fetchMessages, 1000

  # override
  import: (@options, done) ->
    super()
    @onFinished = _.once done if done?

    #@options =
    #  processStats: false
    #@options = _.assign @options, options unless _.isEmpty options
    console.log "Running import with options: " + JSON.stringify @options, null, 2

    @results = {}
    @earliestMessage = false
    @latestMessage = false
    @totalMessages = 0

    @weekStats = {}
    @domains = {}

    @params =
      q: @categories.map((val) -> "category:#{val}").join ' OR '

    if @options.backfill
      @backImport done
    else
      @importNew done

  # override
  fetchUser: (cb) -> super (email) -> User.load email, (@user) -> cb email

  backImport: (done) -> done()
    #HistoryItem.getUserCoverage @emailAddress, (coverage) ->
    #  if coverage.first.error?

  # Import any new items in current year
  importNew: (done) ->
    HistoryItem.getLatestEmail @emailAddress, (lastEmailDate) =>
      unless @options.startDate?
        if lastEmailDate
          lastEmailMoment = moment lastEmailDate
          if lastEmailMoment.isSame moment(), 'day'
            console.log "Already imported e-mails for #{@emailAddress} today"            
            return @finished()
          @options.startDate = lastEmailMoment
        else
          @options.startDate = moment().startOf 'year'
      @params.q += ' after:' + @options.startDate.format 'YYYY/MM/DD'
      if @options.endDate?
        @params.q += ' before:' + @options.endDate.format 'YYYY/MM/DD'
      @fetchMessages @params

  processMessages: (messages, done) ->
    console.log "Processing #{messages.length} messages"
    Message.find
      message_id:
        $in: _.pluck messages, 'id'
    .exec (err, result) =>
      if err? or not result.length
        @insertMessages messages, done
      else
        foundIds = _.pluck result, 'message_id'
        newMessages = _.filter _.compact(messages), (val) -> val.id not in foundIds
        newMessageIds = _.pluck newMessages, 'id'
        @insertMessages newMessages, done

  addStats: (rawDomain, received) ->
    #console.log "Recording stats from message received #{received.format()} from #{rawDomain} in week #{@currentWeek} of #{@currentYear}"
    domain = parseDomain rawDomain
    @domains[domain] ?=
      subdomains: {}
      earliest: received
      latest: received
    @domains[domain].subdomains[rawDomain] = 1 unless domain is rawDomain
    @domains[domain].earliest = received unless @domains[domain].earliest.isBefore received
    @domains[domain].latest = received unless @domains[domain].latest.isAfter received
    @weekStats[domain] ?= 0
    @weekStats[domain]++
    @updateDateRange received

  saveStats: () ->
    console.log "Saving stats for week #{@currentWeek} of #{@currentYear}"
    week = @currentWeek
    year = @currentYear
    addCount = (count, domain, cb) => WeekDomainCount.add @emailAddress, domain, week, year, count, cb
    async.forEachOf @weekStats, addCount, (err) => 
      console.log if err? then "Error saving stats: #{err}" else "Saved stats for week #{week} of #{year}"
    @weekStats = {}

  insertMessages: (messages, done) -> 
    async.mapLimit messages, @concurrency
    , (message, cb) =>
      return cb "Message #{message?.id} missing header" unless message?.payload?
      headers = message.payload.headers      
      from = stripName getHeader headers, 'from' 
      domain = from.split('@').pop()
      rawDate = received = getHeader headers, 'date'
      found = received.match /(?:Mon|Tue|Wed|Thu|Fri|Sat|Sun)?,? ?(.+)(?: \(.+\))?$/
      if found?
        received = moment found[0], "DD MMM YYYY HH:mm:ss ZZ"
      else
        console.log "UNMATCHED DATE FORMAT: #{received}"
        received = moment received #fall back on deprecated guessing behavior
      if received.isBefore @options.startDate
        console.log "Got message #{received.format()} before startDate: #{@options.startDate}"
        return done()
      week = received.week()
      year = received.year()
      changed = false
      changed = true unless @currentYear? and year is @currentYear
      changed = true unless @currentWeek? and week is @currentWeek      
      if changed
        @saveStats()
        console.log "Now importing messages in week #{week} of #{year}"
        @currentWeek = week
        @currentYear = year
      @addStats domain, received
      if @results[domain]? then @results[domain]++ else @results[domain] = 1
      message =
        to_email: @emailAddress
        from_domain: domain
        root_domain: parseDomain domain
        received: received.toDate()
        message_id: message.id
        thread_id: message.thread_id
        header: _.object headers.map (header) -> [header.name, header.value]
      Message.insert message, (err, result) =>
        if err?
          console.log "Error saving messages: #{err}"
        else
          ++@totalMessages
        cb null, result
    , (err, savedMessages) =>
      done err

  updateDateRange: (received) ->
    unless @earliestMessage and @latestMessage
      @earliestMessage = received
      @latestMessage = received
    else if @latestMessage.isBefore received
      @latestMessage = received
    else if @earliestMessage.isAfter received
      @earliestMessage = received
    #console.log "Date range is now #{@earliestMessage.format()} to #{@latestMessage.format()}"

  # override
  finished: () ->
    super()
    console.log "Finished import for #{@emailAddress}"
    @processDomains () => @importComplete()

  processDomains: (done) ->
    update = (props, domain, cb) -> Domain.update domain, props, cb
    async.forEachOf @domains, update, (err) ->
      console.log if err? then "Error processing domains: #{err}" else "Processed all domains"
      done()

  importComplete: () ->
    result =
      email: @emailAddress
      total_messages: @totalMessages
    if @options?.startDate? or @options?.endDate?
      result.dateQuery =
        start: @options?.startDate?.toDate()
        end: @options?.endDate?.toDate()
    result.error = @error if @error  
    unless @totalMessages and @earliestMessage and @latestMessage
      console.log "No new emails imported for #{@emailAddress}"
      if not @error and not @options?.endDate and @options?.startDate.isSame moment().startOf('year'), 'week'
        console.log "*** WARNING *** No emails for #{@emailAddress} in full import of #{moment().year()}"
        result = _.assign result,
          first_email: moment().startOf 'year'
          last_email: moment()
        HistoryItem.insert result, (ok) =>
          console.log "HistoryItem insert OK" if ok
          @onFinished result if @onFinished?
      else
        @onFinished result if @onFinished?
    else
      console.log "Imported emails between #{@earliestMessage.format()} and #{@latestMessage.format()}"
      # Calculate stats for this import
      result = _.assign result,        
        import_start: @importStart
        first_email: @earliestMessage
        last_email: @latestMessage
        total_domains: _.keys(@results).length      
      HistoryItem.insert result, (ok) =>
        console.log "HistoryItem insert OK" if ok
        @onFinished result if @onFinished?

  writeCsv: () ->
    console.log "Total domains detected: " + _.keys(@results).length
    sorted = _.sortBy _.pairs(@results), (n) -> -n[1]
    console.log "Writing #{sorted.length} items to #{@emailAddress}.csv"
    stream = fs.createWriteStream "#{@emailAddress}.csv"
    stream.once 'open', () ->
      stream.write n.join(',') + "\n" for n in sorted
      stream.end()
      console.log "Import complete."
      process.exit 0
  
