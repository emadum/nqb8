es = require 'event-stream'
_ = require 'lodash'
JSONStream = require 'JSONStream'
gcloud = require 'gcloud'

Message = require '../../models/Message'

module.exports =
class MailHuntBigQuery
  constructor: (projectId, dataset, keyFilename) ->
    bq = gcloud.bigquery
      projectId: projectId
      keyFilename: keyFilename
    @dataset = bq.dataset dataset

  transform: (data) ->
    data.header = _.transform data.header, (result, value, name) ->
      result.push
        name: name
        value: value
    , []
    data

  createTable: (tableName, ready) ->
    schema = []
    schema.push
      name: 'message_id'
      type: 'STRING'
      description: 'GMail message ID for this message'
    schema.push
      name: 'header'
      type: 'RECORD'
      mode: 'REPEATED'
      description: 'Raw headers for this message'
      fields: [
        name: 'name'
        type: 'STRING'
      ,
        name: 'value'
        type: 'STRING'
      ]
    @dataset.createTable
      id: tableName
      schema: {fields: schema}
    , (err, table, apiResponse) ->
      ready table

  getTableMetadata: (tableName, result) ->
    table = @dataset.table tableName
    result undefined unless table?
    table.getMetadata (err, metadata, apiResponse) ->
      result metadata

  waitForDone: (job, done) ->
    job.getMetadata (err, metadata) =>
      changed = if metadata.status.state is @jobStatus then false else true
      @jobStatus = metadata.status.state
      switch @jobStatus
        when 'PENDING', 'RUNNING'
          console.log "Job is #{metadata.status.state} after #{@totalRunTime} seconds..."
          @delay = if changed then @initialDelay else @delay * 2
          @totalRunTime += @delay / 1000
          setTimeout (() => @waitForDone job, done), @delay
        when 'DONE' then done metadata
        else console.log "No handler for status: #{@jobStatus}"

  getCount: (cb) -> 
    Message.find
      header:
        $exists: true
    .count (err, count) -> cb count

  importMessages: (done) ->
    @getTableMetadata 'messages', (metadata) =>
      if metadata?
        @importBatched 10000, done
      else
        @createTable 'messages', (table) =>
          @importBatched 10000, done

  runImport: (table, limit, skip, done) ->
    queryStream = Message.find
      header:
        $exists: true
    .skip skip
    .limit limit 
    .select
      message_id: 1
      header: 1
      _id: 0
    .stream()

    jsonStream = queryStream
    # transform the data into the proper format for insertion into BigQuery
    .pipe es.map (data, cb) => cb null, @transform data
    # And turn it into JSON
    .pipe JSONStream.stringify false

    # Display JSON (debug)
    #jsonStream.on 'data', (json) -> console.log json
    jsonStream.on 'error', (err) -> console.log "query stream error #{err}"
    jsonStream.on 'close', () -> console.log "query stream closed"

    # Stream transformed JSON into BigQuery import job
    bqStream = jsonStream.pipe table.createWriteStream 'json'
    bqStream.on 'complete', (job) =>
      console.log "Data upload complete"
      @waitForDone job, done
    bqStream.on 'error', (err) -> console.log "bqStream error #{err}"

  importBatched: (batchSize, done) ->
    @jobStatus = 'IDLE'
    @initialDelay = @delay = 2000
    @totalRunTime = 0
    imported = 0
    table = @dataset.table 'messages'
    @getCount (count) =>
      console.log "Importing #{count} messages into BigQuery..."
      importNext = () =>
        console.log "Importing next batch of #{batchSize} messages..."
        @delay = @initialDelay
        @runImport table, batchSize, imported, (jobResult) =>
          console.log "BigQuery input job completed (skip=#{imported}) after #{@delay} seconds"
          #console.log JSON.stringify jobResult, null, 2
          if _.has jobResult, 'statistics.load.outputRows'
            imported += parseInt jobResult.statistics.load.outputRows
          else
            console.log 'jobResult did not contain load.outputRows statistic'
          if imported < count
            importNext()
          else
            console.log "Imported #{imported}/#{count} records"
            @clearHeaders done
      importNext()
    
  clearHeaders: (done) ->
    Message.update {}, {$unset: {header: 1}}, {multi: true}, (err, numAffected) ->
      console.log if err? then "Error: #{err}" else "Cleared headers from #{numAffected.nModified} messages"
      done()
