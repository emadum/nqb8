moment = require 'moment'
async = require 'async'
_ = require 'lodash'

Message = require '../../models/Message'
Domain = require '../../models/Domain'
WeekDomainCount = require '../../models/WeekDomainCount'

parse_domain = require 'parse-domain'
parseDomain = _.memoize (domain) ->
  parts = parse_domain domain.split(" ")[0]
  unless parts?
    console.log "Unable to parse domain #{domain}"
    domain
  else
    parts.domain + '.' + parts.tld

domains = {}

processMessages = (targetWeek, targetYear, result, done) ->
  console.log "Processing #{result.length} messages in week #{targetWeek},#{targetYear}"
  counts = {}  
  for message in result
    domain = parseDomain message.from_domain
    domains[domain] ?=
      total_messages: 0
      earliest: message.received
      latest: message.received
    domains[domain].total_messages++
    unless domain is message.from_domain      
      if typeof domains[domain].subdomains is 'undefined'
        domains[domain].subdomains = new Object()
      unless typeof domains[domain].subdomains is 'undefined'
        domains[domain].subdomains[message.from_domain] = 1
      else
        #console.log "Still undefined"
    if message.received < domains[domain].earliest
      domains[domain].earliest = message.received
    else if message.received > domains[domain].latest
      domains[domain].latest = message.received
    # Track counts
    counts[message.to_email] ?= {}
    counts[message.to_email][message.from_domain] ?= 0
    ++counts[message.to_email][message.from_domain]
  # Insert WeekDomainCounts
  inserts = []
  getCallback = (email, domain, count) ->
    (cb) ->
      WeekDomainCount.remove
        to_email: email
        from_domain: domain
        week: targetWeek
        year: targetYear
      , (err) ->
        WeekDomainCount.insert
          to_email: email
          from_domain: domain
          count: count
          week: targetWeek
          year: targetYear
        , cb
  for email, subdomains of counts
    for domain, count of subdomains
      inserts.push getCallback email, domain, count
  async.parallel inserts, (err, result) ->
    console.log unless err? then "Inserted #{result.length} counts" else "Error inserting counts: #{err}"
    done null, "Processed #{result.length} messages in week #{targetWeek},#{targetYear}"

processWeek = (n, year, done) ->
  targetWeek = n
  start = moment().year(year).week(targetWeek).startOf 'week'
  end = moment().year(year).week(targetWeek).add(1,'w').startOf 'week'
  Message.find
    received:
      $gte: start.toDate()
      $lte: end.toDate()
  .exec (err, result) ->
    if err? then done err else processMessages targetWeek, year, result, done

saveDomainCallback = (domain, props) -> (cb) -> 
  Domain.findOne
    domain: domain
  .exec (err, result) ->
    if result?
      needSave = false
      if result.subdomains? and result.subdomains.length
        #console.log "Existing subdomains: " + result.subdomains.join ','
      else if props.subdomains? and props.subdomains.length
        result.subdomains = props.subdomains
        needSave = true
      if result.earliest > props.earliest
        result.earliest = props.earliest
        needSave = true
      if result.latest < props.latest
        result.latest = props.latest
        needSave = true
      if needSave then result.save cb else cb null, "No change for #{domain}"
    else
      console.log "Error finding domain: #{err}" if err?
      console.log "Inserting new domain: #{domain}"
      props.domain = domain
      props.subdomains = _.keys props.subdomains if props.subdomains?
      Domain.insert props, cb

processRange = (start, end, done) ->
  weekRanges = getWeekRanges start, end
  console.log "Week ranges:" + JSON.stringify weekRanges
  process = (weekRange, cb) ->
    console.log "Processing stats for weeks #{weekRange.weeks.join ','} in year #{weekRange.year}"
    processWeeks weekRange.weeks, weekRange.year, () -> 
      console.log "Done processing weeks in #{weekRange.year}"
      cb()
  async.eachSeries weekRanges, process, done

processWeeks = (weeks, year, done) ->
  console.log "Processing weeks: #{weeks.join ','}"
  process = (n, cb) -> processWeek n, year, cb
  async.mapSeries weeks, process, (err, result) ->
    console.log "processWeeks result: (err:#{err})"
    console.log line for line in result
    # Insert Domains
    saveDomains = []
    for domain, props of domains
      saveDomains.push saveDomainCallback domain, props
    async.parallel saveDomains, (err2, result2) ->
      console.log if err2? then err2 else "Saved #{result2.length} domains"
      done()

processYear = (year, done) ->
  finalWeek = if year is moment().year() then moment().week() else lastWeekInYear year
  processWeeks [1 .. finalWeek], year, done

updateFirstUse = (props, cb) ->
  WeekDomainCount.find props
  .exec (err, wdc) ->
    if wdc?.length
      wdc[0].first_use = true
      wdc[0].save()
      cb null
    else
      cb err

analyzeNewUsers = (range, finish) ->
  console.log "Analyzing users in range " + JSON.stringify range
  query =
    latest:      
      $gte: range.after.toDate()
  query.latest.$lte = range.before.toDate() if range.before?
  domains = Domain.find(query).stream()
  domains.on 'data', (domain) ->
    WeekDomainCount.getWeeklyNewUsers domain.domain, (result) ->
      async.forEachOf result, (item, email, cb) ->
        updateFirstUse
          to_email: email
          from_domain: domain.domain
          year: item.year
          week: item.week
        , cb
      , (err) -> console.log "Updated new users for #{domain.domain}"
  domains.on 'close', finish

lastWeekInYear = (year) ->
  end = moment([year]).year(year).endOf 'year'
  end = end.subtract 1, 'd' while end.week() is 1
  end.week()

getWeekRanges = (start, end) ->
  startWeek = start.week()
  startYear = start.endOf('week').year()
  endWeek = end.week()
  endYear = end.startOf('week').year()
  console.log "Finding ranges for #{startWeek},#{startYear}-#{endWeek},#{endYear}"
  ranges = []
  if startYear is endYear
    # partial year
    ranges.push
      weeks: [startWeek .. endWeek]
      year: startYear
  else
    # first year from startWeek through end of year
    ranges.push
      weeks: [startWeek .. lastWeekInYear startYear]
      year: startYear
    # full intermediate years
    if endYear - startYear > 1
      for year in [startYear+1 .. endYear-1]
        ranges.push
          weeks: [1 .. lastWeekInYear year]
          year: year
    # last year up to endWeek
    ranges.push
      weeks: [1 .. endWeek]
      year: endYear
  ranges

module.exports =
  processRange: processRange
  processWeeks: processWeeks
  processYear: processYear
  analyzeNewUsers: analyzeNewUsers
