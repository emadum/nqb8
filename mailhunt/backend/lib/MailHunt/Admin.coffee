_ = require 'lodash'
path = require 'path'
async = require 'async'
moment = require 'moment'
child_process = require 'child_process'

User = require '../../models/User'
GmailAuth = require '../../models/GmailAuth'
HistoryItem = require '../../models/HistoryItem'
Message = require '../../models/Message'
BatchImport = require '../../models/BatchImport'

MailHuntImporter = require './Importer'

MailHuntBigQuery = require './BigQuery'
saveToBigQuery = (done) ->
  console.log "Starting import into BigQuery"
  c = config.bigquery
  mhbq = new MailHuntBigQuery c.project_id, c.dataset, path.resolve __dirname, '..', '..', c.key_file
  mhbq.importMessages () ->
    console.log "Finished BigQuery import"
    done()

Stats = require './Stats'

config = require '../../config'

spawnImport = (command, done) ->
  importCmd = command.join ' '
  console.log "Running #{importCmd}"
  child_process
  .spawn "#{__dirname}/../../scripts/mailhunt", command
  .stdout.on 'data', (data) ->
    process.stdout.write data
  .on 'close', (code) ->
    console.log "Finished `#{importCmd}` with code #{code}"
    done()

importUserOpts = (user, opts, done) ->
  mailHunt = new MailHuntImporter config.googleapi
  mailHunt.loadSavedAuth user, (err, ok) ->
    if ok is true
      mailHunt.fetchUser (email) ->
        if user is email
          console.log "Importing for #{email}..."
          mailHunt.import opts, done
        else
          done
            email: email
            error: "Auth issues; attempted login with #{user} but got #{email}"
    else
      done
        email: user
        error: "Auth failed: #{err}"

importUser = (email, done) ->
  console.log "Starting import for #{email}"
  spawnImport ['import', email], done

importUserBetween = (email, startDate, endDate, done) ->
  start = startDate.format 'W,YYYY'
  end = endDate.format 'W,YYYY'
  console.log "Starting import for #{email} between #{start} and #{end}"
  spawnImport ['import', email, '-s', start, '-e', end], done

backfillUser = (email, toDate, done) ->
  HistoryItem.find
    email: email
  .sort
    first_email: 1
  .exec (err, history) ->
    unless history.length
      console.log "#{email} still needs initial import"
      return done()
    first = moment history[0].first_email
    if first.isBefore toDate
      console.log "#{email} already current through #{toDate.format('YY-M-D')}"
      return done()    
    console.log "#{email} needs import from #{toDate.format('YY-M-D')} to #{first.format('YY-M-D')}"
    importUserBetween email, toDate, first, done

filterExisting = (auth, result) ->
  HistoryItem.find
    email: auth.email
  .exec (err, history) ->
    result if history.length then false else true

filterAlreadyImported = (auth, result) ->
  HistoryItem.find
    email: auth.email
  .sort
    import_finish: -1
  .exec (err, history) ->
    if history.length
      result if moment(history[0].import_finish).isSame moment(), 'day' then false else true
    else
      result false

noFilter = (auth, result) -> result true

filterUsers = (filter, users) ->
  GmailAuth.find
    valid: true
  .exec (err, auths) ->
    async.filter auths, filter, (results) ->
      users _.pluck results, 'email'

getNewUsers = (users) -> filterUsers filterExisting, users

usersNeedUpdate = (users) -> filterUsers filterAlreadyImported, users

getAllUsers = (users) -> filterUsers noFilter, users

runQueued = (users, command, done) ->
  q = async.queue command, config.import.concurrency
  q.drain = done
  q.push users

importUsers = (users, done) ->
  numUsers = users.length
  start = new Date()
  runQueued users, importUser, () ->
    BatchImport.save
      num_users: numUsers
      import_start: start
    , done

backfillUsers = (users, toDate, done) ->
  backfill = (email, done) -> backfillUser email, toDate, done
  runQueued users, backfill, done

confirmAuth = (user, cb) ->
  mailHunt = new MailHuntImporter config.googleapi
  mailHunt.loadSavedAuth user, (err, ok) ->
    if ok is true
      mailHunt.fetchUser (email) ->
        if user is email
          cb 'OK'
        else
          cb "Tried to log in as #{user}, logged in as #{email}"
    else
      cb err

confirmAllAuths = (done) ->
  GmailAuth.find
    valid: true
  .select
    email: 1
    _id: 0
  .exec (err, results) ->
    async.map results
    , (item, cb) ->
      confirmAuth item.email, (result) -> cb null, [item.email, result]
    , (err, final) ->      
      done final

module.exports =
  auth:
    confirm: confirmAuth
    confirmAll: confirmAllAuths      
  import:
    user: importUserOpts
    userSpawned: (email, opts, done) ->
      cmd = ['import', email]
      if opts.startDate?
        start = opts.startDate.format 'W,YYYY'
        cmd = cmd.concat ['-s', start]
      if opts.endDate?
        end = opts.endDate.format 'W,YYYY'
        cmd = cmd.concat ['-e', end]      
      spawnImport cmd, done
    backfillAll: (toDate) ->
      getAllUsers (users) -> backfillUsers users, moment(toDate), done
    newUsers: (done) ->
      getNewUsers (users) -> importUsers users, done
    update: (done) ->
      usersNeedUpdate (users) -> importUsers users, done
  importStatus: (email, cb) ->
    HistoryItem.getUserCoverage email, cb
  export:
    messages: (outputFile, done) -> Message.exportCsv outputFile, done
  analyze:
    newUsers: (range) ->
      Stats.analyzeNewUsers range
      , (err, result) ->
        console.log JSON.stringify result
        process.exit 0
    counts: (year) -> Stats.processYear year, () -> process.exit 0
