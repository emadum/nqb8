async = require 'async'
_ = require 'lodash'
moment = require 'moment'

Domain = require '../../models/Domain'
User = require '../../models/User'
Message = require '../../models/Message'

findNewApps = (user, cb) ->
  Message.newDomainsInLast 30, user, (newDomains) ->
    cb null, newDomains

findTrendingApps = (ret) ->
  User.forAll findNewApps, (newApps) ->
    totals = {}
    for email, apps of newApps      
      for app in apps
        totals[app] ?= 0
        totals[app]++
    ret _.sortBy _.pairs(totals), (item) -> -item[1]

onlyNewApps = (app, cb) ->
  Domain.findOne
    domain: app[0]
  .exec (err, result) ->
    if err? or not result?
      console.log "No domain: #{app[0]} err:#{err}"
      cb true
    else
      cb moment(result.earliest).isAfter moment().subtract 30, 'd'

addDomainInfo = (apps, ret) ->
  domains = _.pluck apps, 'domain'
  Domain.getAll domains, (info) ->
    console.log JSON.stringify info, null, 2
    i = 0
    ret _.map apps, (item) ->
      index: ++i
      domain: item.domain
      count: item.count
      logo_url: info[item.domain].logo
      organization: info[item.domain].organization
      description: info[item.domain].description

module.exports =

  trendingApps: (ret) ->
    findTrendingApps (apps) ->
      trending = _.map apps.slice(0, 100), (item) ->
        domain: item[0]
        count: item[1]
      addDomainInfo trending, ret

  popularApps: (ret) ->
    Message.popularInLast 30, (apps) ->    
      popular = _.map apps, (item) ->
        domain: item._id
        count: item.newUsers
      addDomainInfo popular, ret

  newApps: (ret) ->
    findTrendingApps (apps) ->
      async.filter apps, onlyNewApps, (filtered) ->
        newApps = _.map filtered.slice(0,100), (item) ->
          domain: item[0]
          count: item[1]
        addDomainInfo newApps, ret
