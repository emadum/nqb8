async = require 'async'
moment = require 'moment'
_ = require 'lodash'

config = require '../config'

mongoose = require 'mongoose'
mongoose.connect "mongodb://#{config.mongodb.host}/#{config.mongodb.database}", (err) ->
  console.log "Error connecting to Mongo: ", err if err?

User = require '../models/User'
GmailAuth = require '../models/GmailAuth'
HistoryItem = require '../models/HistoryItem'

child_process = require 'child_process'
concurrency = 4

spawnImport = (command, done) ->
  importCmd = command.join ' '
  console.log "Running #{importCmd}"
  child_process
  .spawn "#{__dirname}/mailhunt", command
  .stdout.on 'data', (data) ->
    process.stdout.write data
  .on 'close', (code) ->
    console.log "Finished `#{importCmd}` with code #{code}"
    done()

importUser = (email, done) ->
  console.log "Starting import for #{email}"
  spawnImport ['import', email], done

importUserBetween = (email, startDate, endDate, done) ->
  start = startDate.format 'W,YYYY'
  end = endDate.format 'W,YYYY'
  console.log "Starting import for #{email} between #{start} and #{end}"
  spawnImport ['import', email, '-s', start, '-e', end], done

backfillUser = (email, toDate, done) ->
  HistoryItem.find
    email: email
  .sort
    first_email: 1
  .exec (err, history) ->
    unless history.length
      console.log "#{email} still needs initial import"
      return done()
    first = moment history[0].first_email
    if first.isBefore toDate
      console.log "#{email} already current through #{toDate.format('YY-M-D')}"
      return done()    
    console.log "#{email} needs import from #{toDate.format('YY-M-D')} to #{first.format('YY-M-D')}"
    importUserBetween email, toDate, first, done

filterExisting = (auth, result) ->
  HistoryItem.find
    email: auth.email
  .exec (err, history) ->
    result if history.length then false else true

filterAlreadyImported = (auth, result) ->
  HistoryItem.find
    email: auth.email
  .sort
    import_finish: -1
  .exec (err, history) ->
    if history.length
      result if moment(history[0].import_finish).isSame moment(), 'day' then false else true
    else
      result false

noFilter = (auth, result) -> result true

filterUsers = (filter, users) ->
  GmailAuth.find
    valid: true
  .exec (err, auths) ->
    async.filter auths, filter, (results) ->
      users _.pluck results, 'email'

getNewUsers = (users) -> filterUsers filterExisting, users

usersNeedUpdate = (users) -> filterUsers filterAlreadyImported, users

getAllUsers = (users) -> filterUsers noFilter, users

runQueued = (users, command) ->
  q = async.queue command, concurrency
  q.drain = () ->
    console.log "Done processing imports"
    process.exit 0
  q.push users

importUsers = (users) -> runQueued users, importUser

backfillUsers = (users, toDate) ->
  runQueued users, (email, done) ->
    backfillUser email, toDate, done

#getAllUsers (users) -> backfillUsers users, moment '2014/01/01'
#getNewUsers (users) -> importUsers users
usersNeedUpdate (users) -> importUsers users
