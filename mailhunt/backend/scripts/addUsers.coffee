async = require 'async'

config = require '../config'

mongoose = require 'mongoose'
mongoose.connect "mongodb://#{config.mongodb.host}/#{config.mongodb.database}", (err) ->
  console.log "Error connecting to Mongo: ", err if err?

User = require '../models/User'
GmailAuth = require '../models/GmailAuth'

GmailAuth.find().exec (err, results) ->
  done = 0
  addUser = (item, cb) ->
    newUser = new User
      email: item.email
      joined: item.joined
    newUser.is_admin = item.is_admin if item.is_admin?
    newUser.save (err) ->
      done++
      cb err
  async.each results, addUser, (err) ->
    if err?
      console.log "Error updating users: #{err}"
    else
      console.log "Updated #{done}/#{results.length} users"
    process.exit 0

