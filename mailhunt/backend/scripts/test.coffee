config = require '../config'

mongoose = require 'mongoose'
mongoose.connect "mongodb://#{config.mongodb.host}/#{config.mongodb.database}", (err) ->
  console.log "Error connecting to Mongo: ", err if err?

meta = require 'metatags'
_ = require 'lodash'
Bottleneck = require 'bottleneck'

Domain = require '../models/Domain'

limiter = new Bottleneck 5

total = 0
finished = 0
done = () ->
  if ++finished is total
    console.log "Done processing all domains"
    process.exit 0
  else
    console.log "Analyzed #{finished}/#{total} domains"
Domain.find
  organization:
    $exists: false
  description:
    $exists: false
.stream()
.on 'data', (doc) ->
  total++
  limiter.submit meta, "http://www.#{doc.domain}", (err, data) ->
    return done() if err or not data?
    org = _.compact([data.author, data.title.split(' | ').pop()]).shift()
    desc = data.description if data.description?
    if org? or desc?
      doc.organization = org if org?
      doc.description = desc if desc?
      doc.save (err) -> console.log "Error saving domain metadata: #{err}" if err?
    done()
.on 'close', () ->
  console.log "Analyzing #{total} domains..."


# async = require 'async'
# moment = require 'moment'



# User = require '../models/User'
# Message = require '../models/Message'

# User.forAll Message.getUserDateRange, (result) ->
#   console.log JSON.stringify result, null, 2
#   for user, range of result
#     if range?
#       console.log "#{user} has messages going back #{moment(range.first).fromNow()}"
#     else
#       console.log "#{user} has no messages"
#   process.exit 0


# parse_domain = require 'parse-domain'
# parseDomain = _.memoize (domain) ->
#   return domain unless domain?
#   parts = parse_domain domain.split(" ")[0]
#   unless parts?
#     console.log "Unable to parse domain #{domain}"
#     domain
#   else
#     parts.domain + '.' + parts.tld

# Message = require '../models/Message'

# Message.find
#   root_domain:
#     $exists: false
# .stream()
# .on 'data', (message) ->
#   message.root_domain = parseDomain message.from_domain
#   message.save (err) -> console.log err if err?
# .on 'close', () ->
#   console.log "Done adding root domains"
