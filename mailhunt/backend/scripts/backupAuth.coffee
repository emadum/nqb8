mongoose = require 'mongoose'
mongoose.connect 'mongodb://localhost/mailhunt', (err) ->
console.log "Error connecting to Mongo: ", err if err?
_ = require 'lodash'
async = require 'async'

MailHuntImporter = require '../lib/MailHunt/Importer'
GmailAuth = require '../models/GmailAuth'

config = require '../config'

GmailAuth.find().exec (err, results) ->  
  backup = {}
  async.each results
  , (item, cb) ->
    backup[item.email] =
      access_token: item.access_token
      refresh_token: item.refresh_token 
    cb()
  , (err) ->
    console.log JSON.stringify backup, null, 2
    process.exit 0
