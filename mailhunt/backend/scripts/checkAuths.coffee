mongoose = require 'mongoose'
mongoose.connect 'mongodb://localhost/mailhunt', (err) ->
  console.log "Error connecting to Mongo: ", err if err?
_ = require 'lodash'
async = require 'async'

MailHuntImporter = require '../lib/MailHunt/Importer'
GmailAuth = require '../models/GmailAuth'

config =require '../config'

confirmAuth = (user, cb) ->
  mailHunt = new MailHuntImporter config.googleapi
  mailHunt.loadSavedAuth user, (err, ok) ->
    if ok is true
      mailHunt.fetchUser (email) ->
        if user is email
          cb 'OK'
        else
          cb "Tried to log in as #{user}, logged in as #{email}"
    else
      cb err
        
GmailAuth.find
  valid: true
.select
  email: 1
  _id: 0
.exec (err, results) ->
  async.map results
  , (item, cb) ->
    confirmAuth item.email, (result) -> cb null, [item.email, result]
  , (err, final) ->
    console.log JSON.stringify _.object(final), null, 2
    process.exit 0
