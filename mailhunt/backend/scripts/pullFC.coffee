mongoose = require 'mongoose'
mongoose.connect 'mongodb://localhost/mailhunt', (err) ->
console.log "Error connecting to Mongo: ", err if err?

Domain = require '../models/Domain'
Company = require '../models/Company'

actions = []

getDomainInfo = (domain) -> () -> Domain.get domain, (info) -> console.log JSON.stringify info

processNext = () ->
  if actions.length
    action = actions.pop()
    action()

Domain.find().stream().on "data", (domain) ->
  Company.findOne
    domain: domain.domain
  .exec (err, company) ->
    return  if company?
    #fc = company.fullContact.response
    #return if fc.status in [200, 202, 404]
    actions.push getDomainInfo domain.domain

setInterval processNext, 1100
