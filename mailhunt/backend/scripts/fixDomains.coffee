_ = require 'lodash'

config = require '../config'

mongoose = require 'mongoose'
mongoose.connect "mongodb://#{config.mongodb.host}/#{config.mongodb.database}", (err) ->
  console.log "Error connecting to Mongo: ", err if err?

Domain = require '../models/Domain'

updateDomain = (combined) ->
  #console.log JSON.stringify combined
  domain = combined.domain
  Domain.remove
    domain: domain
  , (err) ->
    if err?
      console.log "Error removing old entry for #{domain}"
    else
      console.log "Removed old entry for #{domain}"
    Domain.insert combined, (err) ->
      console.log "Inserted new entry for #{domain}"

match =
  $match: {}
group =
  $group:
    _id: "$domain"
    earliest:
      $min: "$earliest"
    latest:
      $max: "$latest"
    total_messages:
      $max: "$total_messages"
    subdomains:
      $addToSet: "$subdomains"
    organization:
      $addToSet: "$organization"
    description:
      $addToSet: "$description"
    logo:
      $addToSet: "$logo"
sort =
  $sort:
    _id: 1
#console.log "Aggregating: " + JSON.stringify [match, group, sort], null, 2
Domain.aggregate [match, group, sort]
.exec (err, results) ->
  if err? or not results.length
    console.log "No results, err: #{err}"
    return
  for result in results
    domain = result._id
    combined =
      domain: domain
    for field in ['earliest', 'latest', 'total_messages']
      combined[field] = result[field]
    for field in ['subdomains', 'organization', 'description', 'logo']
      combined[field] = _.union.apply _, result[field]
      combined[field] = result[field].shift() unless field is 'subdomains'
    updateDomain combined
    