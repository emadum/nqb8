mongoose = require 'mongoose'

schema = new mongoose.Schema
    user: String
    thread_id: String
    thread_count: Number
    history_id: Number

schema.statics.checkForNewMessages = (email, thread, cb) ->
	query = Thread.find
		user: email
		thread_id: thread.id
	query.exec (err, docs) ->
		result = true
		savedThread = null
		if docs.length > 0
			if parseInt(docs[0].history_id) is parseInt(thread.historyId)
				result = false
				savedThread = docs[0]
			else
				console.log "Thread was updated; old history=#{docs[0].history_id}, new history=#{thread.historyId}"
				result = true
		cb result, savedThread

schema.statics.insertThread = (doc, cb) ->
    ins = new Thread doc
    ins.save (err) ->
    	cb if err then false else true

module.exports = Thread = mongoose.model 'Thread', schema  