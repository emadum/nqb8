moment = require 'moment'
mongoose = require 'mongoose'

schema = new mongoose.Schema
  email: String
  dateQuery:
    start: Date
    end: Date
  total_domains: Number
  total_messages: Number
  first_email: Date
  last_email: Date
  import_start: Date
  import_finish: Date
  error: String

schema.statics.insert = (doc, result) ->
  doc.import_finish = new Date()
  historyItem = new HistoryItem doc	
  historyItem.save (err) ->
    if err?
      console.log "Error creating history item:" + JSON.stringify doc, null, 2
      console.log err
      result false
    else
      result true

schema.statics.getLatestImport = (email, cb) ->
  HistoryItem.find
    email: email
  .exists 'last_email'
  .sort
    import_finish: -1
  .exec (err, docs) ->
    cb if err? or not docs.length then false else docs[0].last_email

schema.statics.getLatestEmail = (email, cb) ->
  HistoryItem.find
    email: email
  .exists 'last_email'
  .sort
    last_email: -1
  .exec (err, docs) ->
    cb if err? or not docs.length then false else docs[0].last_email

schema.statics.getUserCoverage = (email, cb) ->
  HistoryItem.find
    email: email
  .exec (err, imports) ->
    return cb err if err?
    result =
      status: 'need_import'
    return cb null, result unless imports.length
    result.first = 
      error: imports[0].error
      moment: moment imports[0].first_email  
    result.last = 
      error: imports[0].error
      moment: moment imports[0].last_email
    for item in imports
      first = moment item.first_email
      last = moment item.last_email
      if result.first.moment.isAfter first
        result.first.moment = first
        result.first.error = item.error
      if result.last.moment.isBefore last
        result.last.moment = last
        result.last.error = item.error
    cb null, result

schema.statics.getUserHistory = (email, cb) ->
  HistoryItem.find
    email: email
  .sort
    import_finish: 1
  .exec (err, historyItems) ->
    return cb false if err? or not historyItems.length
    firstImport = historyItems[0]
    firstEmail = moment firstImport.first_email
    lastEmail = moment firstImport.last_email
    lastImport = historyItems[historyItems.length-1]
    firstEmailError = firstImport.error
    domains = 0
    messages = 0
    for item in historyItems
      domains += item.total_domains
      messages += item.total_messages
      first = moment item.first_email
      if firstEmail.isAfter first
        firstEmail = first 
        firstEmailError = item.error
      last = moment item.last_email
      lastEmail = last if lastEmail.isBefore last
    cb
      total_domains: domains
      total_messages: messages
      first_email: [firstEmail.format("MMM Do, YYYY"), firstEmailError]
      last_email: lastEmail.format "MMM Do, YYYY"
      last_import_finish: lastImport.import_finish
      last_import_error: lastImport.error
      duration: moment.duration(moment(lastImport.import_finish).diff(moment(lastImport.import_start))).humanize()


module.exports = HistoryItem = mongoose.model 'HistoryItem', schema
