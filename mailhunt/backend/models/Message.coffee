fs = require 'fs'
moment = require 'moment'
mongoose = require 'mongoose'
_ = require 'lodash'

schema = new mongoose.Schema
  message_id:
    type: String
    index:
      unique: true
  to_email: String
  from_domain:
    type: String
    index: true
  root_domain:
    type: String
    index: true
  received: Date
  thread_id: String
  header: mongoose.Schema.Types.Mixed

schema.index
  to_email: 1
  received: -1

schema.statics.insert = (doc, cb) ->
  ins = new Message doc
  ins.save cb

schema.statics.getUserDateRange = (email, cb) ->
  Message.findOne
    to_email: email
  .sort
    received: -1
  .exec (err, last) ->
    return cb err unless last?
    Message.findOne
      to_email: email
    .sort
      received: 1
    .exec (err, first) ->
      return cb err unless first?
      cb null,
        first: first.received
        last: last.received

schema.statics.newDomainsInLast = (days, email, cb) ->
  Message.distinct 'root_domain',
    to_email: email
    received:
      $lt: moment().subtract(days, 'd').toDate()
  , (err, domainsBefore) ->
    return cb [] if err? or not domainsBefore.length
    Message.distinct 'root_domain',
      to_email: email
    , (err, domainsNow) ->
      return cb domainsThisMonth if err? or not domainsNow.length
      cb _.difference domainsNow, domainsBefore

schema.statics.popularInLast = (days, cb) ->
  Message.aggregate()
  .match
    received:
      $gt: moment().subtract(days, 'd').toDate()
  .group
    _id: "$root_domain"
    users:
      $addToSet: "$to_email"
  .project
    newUsers:
      $size: "$users"
  .sort
    newUsers: -1
  .limit 100
  .exec (err, result) -> cb result

schema.statics.removeDupes = () ->
  group =
    $group:
      _id: "$message_id"
      dups:
        $push: "$_id"
      count:
        $sum: 1
  match =
    $match:
      count:
        $gt:1
  Message.aggregate [group, match]
  .exec (err, result) ->
    return console.log "Error: #{err}" if err?
    for doc in result
      doc.dups.shift()
      Message.remove
        _id:
          $in: doc.dups
      , () -> console.log "Removed"

prepare = (data) -> moment(data.received).format('MM/DD/YYYY H:mm:ss') + ',' + data.from_domain + ',' + data.to_email

schema.statics.exportCsv = (outputFile, done) ->
  console.log "Exporting messages to #{outputFile}..."
  out = fs.openSync outputFile, 'w'
  write = (line) -> fs.writeSync out, line+"\n"
  headers = ['from_domain', 'to_email', 'received']
  write headers.join ','
  written = 0
  monitor = setInterval () ->
    console.log "Wrote #{written} messages to csv last second"
    written = 0
  , 1000
  stream = Message.find().stream()
  .on 'data', (data) ->
    write prepare data
    written++
  .on 'end', () -> 
    clearTimeout monitor
    fs.closeSync out
    done()

module.exports = Message = mongoose.model 'Message', schema  