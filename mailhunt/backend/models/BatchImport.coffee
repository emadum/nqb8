mongoose = require 'mongoose'

schema = new mongoose.Schema
  num_users: Number
  import_start: Date
  import_end: Date

schema.statics.save = (props, cb) ->
  props.import_end = new Date()  
  ins = new BatchImport props
  ins.save cb

module.exports = BatchImport = mongoose.model 'BatchImport', schema