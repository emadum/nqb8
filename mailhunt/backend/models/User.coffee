_ = require 'lodash'
async = require 'async'
mongoose = require 'mongoose'

schema = new mongoose.Schema
  email:
    type: String
    index: true
  is_admin: Boolean
  joined: Date
  ref_code: String
  permissions: [String]

schema.statics.forAll = (action, ret) ->
  User.find().exec (err, users) ->  
    emails = _.pluck users, 'email'
    async.map emails, action, (err, results) ->
      ret _.object emails, results

schema.statics.forAllSeries = (action, ret) ->
  User.find().exec (err, users) ->  
    emails = _.pluck users, 'email'
    async.mapSeries emails, action, (err, results) ->
      ret _.object emails, results

schema.statics.set = (email, props, cb) ->
  User.load email, (err, user) ->
    return cb err if err?
    user[k] = v for k, v of props
    user.save cb

schema.statics.load = (email, cb) ->
  User.find
    email: email
  , (err, result) ->
    if err? then cb err
    else if result.length then cb null, result[0]
    else User.insert email, cb

schema.statics.insert = (email, cb) ->
  ins = new User
    email: email
    joined: new Date()
  ins.save cb

schema.statics.get = (email, cb) ->
  User.findOne
    email: email
  .exec cb

schema.statics.grant = (user, permission) ->
  console.log "Granting #{permission} to #{user}"
  User.get email, (record) ->
    unless permission in record.permissions
      permissions.push record
      record.save (err) ->
        console.log "Error granting #{permission} to #{user}: #{err}" if err?
    else
      console.log "#{user} is already authorized for #{permission}"

schema.statics.ungrant = (user, permission) ->
  console.log "Removing #{permission} from #{user}"
  User.get email, (record) ->
    if permission in record.permissions
      record.permissions = _.without record.permissions, permission
      record.save (err) ->
        console.log "Error removing #{permission} from #{user}: #{err}" if err?
    else
      console.log "#{user} is not authorized for #{permission}"

schema.statics.isAdmin = (email, yep, nope) ->
  User.get email, (err, user) ->
    if err? or not user? or not user.is_admin then nope() else yep()

module.exports = User = mongoose.model 'User', schema