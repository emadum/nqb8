mongoose = require 'mongoose'
_ = require 'lodash'
async = require 'async'
meta = require 'metatags'

Company = require './Company'

config = require '../config'

schema = new mongoose.Schema
  domain:
    type: String
    index:
      unique: true
  subdomains: [String]
  earliest: Date
  latest: Date
  total_messages: Number
  organization: String
  description: String
  logo: String
  override: Boolean

schema.statics.insert = (doc, cb) ->
  meta "http://www.#{doc.domain}", (err, data) ->
    console.log JSON.stringify data, null, 2
    doc.description = data.description if data.description?
    ins = new Domain doc
    ins.save cb

schema.statics.update = (domain, props, cb) ->
  Domain.findOne
    domain: domain
  .exec (err, result) ->
    if result?
      needSave = false
      if result.subdomains? and result.subdomains.length
        #console.log "Existing subdomains: " + result.subdomains.join ','
      else if props.subdomains? and props.subdomains.length
        result.subdomains = props.subdomains
        needSave = true
      if result.earliest > props.earliest
        result.earliest = props.earliest
        needSave = true
      if result.latest < props.latest
        result.latest = props.latest
        needSave = true
      if needSave then result.save cb else cb null, "No change for #{domain}"
    else
      console.log "Error finding domain: #{err}" if err?
      console.log "Inserting new domain: #{domain}"
      props.domain = domain
      props.subdomains = _.keys props.subdomains if props.subdomains?
      Domain.insert props, cb

schema.statics.getAll = (domains, cb) ->
  getInfo = (domain, ret) -> Domain.get domain, (info) -> ret null, info
  async.map domains, getInfo, (err, results) ->
    cb _.object domains, results

schema.statics.get = (domain, cb) ->
  Domain.findOne
    domain: domain
  .exec (err, doc) ->
    if err? or not doc?
      cb false
    else
      ret = doc.toObject()
      return cb ret if ret.override
      key = config.fullcontact.api_key
      Company.getFullContact key, domain, (err, company) ->
        unless err? or not company?
          doc.organization = _.get company, 'fullContact.response.organization.name'
          profile = _.get company, 'fullContact.response.overview'
          unless profile?
            profile = _.find company.fullContact.response.socialProfiles, (item) -> item.bio?
            profile = profile.bio if profile?
          doc.description = profile if profile?
          doc.logo = _.get company, 'fullContact.response.logo'
          doc.save (err) -> console.log "Save domain error? #{err}" if err?
          ret = doc.toObject()
          ret.company = company
        cb ret

module.exports = Domain = mongoose.model 'Domain', schema  