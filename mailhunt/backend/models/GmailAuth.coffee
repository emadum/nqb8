mongoose = require 'mongoose'
_ = require 'lodash'

schema = new mongoose.Schema
    email: String
    access_token: String
    refresh_token: String
    expiry_date: Date
    is_admin: Boolean
    joined: Date
    valid: Boolean
    error:
      message: String
      date: Date

schema.statics.save = (doc, cb) ->
	GmailAuth.find
		email: doc.email
	, (err, result) ->
		cb if err then false else true
		if result.length # update existing entry
			result[0] = _.assign result[0], doc
			result[0].save (err) -> cb if err? then false else true
		else GmailAuth.insert doc, cb

schema.statics.insert = (doc, cb) ->
    doc.joined = new Date()
    doc.valid = true
    ins = new GmailAuth doc
    ins.save (err) -> cb if err then false else true

schema.statics.authorizedEmails = (cb) ->
	GmailAuth.find()
	.exists 'refresh_token'
	.select 'email'
	.exec (err, result) ->
		if err?
			console.log "Error getting auth'd emails:" + err
			cb []
		else
			cb _.pluck result, 'email'

module.exports = GmailAuth = mongoose.model 'GmailAuth', schema