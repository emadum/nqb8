mongoose = require 'mongoose'
request = require 'request'
_ = require 'lodash'
Bottleneck = require 'bottleneck'

schema = new mongoose.Schema
  name: String
  domain: 
    type: String
    index:
      unique: true
  fullContact:
    updated: Date
    response: mongoose.Schema.Types.Mixed

limiter = new Bottleneck 1, 1000

statusOk = (doc) ->
  response = _.get doc, 'fullContact.response'
  if response? and response.status in [200, 404] then true else false

schema.statics.getFullContact = (apiKey, domain, cb) ->
  Company.find
    domain: domain
  , (err, result) ->
    if err?
      cb err
    else if result.length and statusOk result[0]
      cb null, result[0]
    else
      limiter.submit request,
        url: "https://api.fullcontact.com/v2/company/lookup.json?domain=#{domain}&apiKey=#{apiKey}"
        json: true
      , (err, res, json) ->
        unless err?
          props =
            name: if json.organization?.name? then json.organization.name else domain
            domain: domain
            fullContact:
              updated: new Date()
              response: json
          if result.length
            result[0][key] = prop for key, prop of props
            result[0].save cb
          else
            ins = new Company props
            ins.save cb

module.exports = Company = mongoose.model 'Company', schema