mongoose = require 'mongoose'
_ = require 'lodash'
async = require 'async'
moment = require 'moment'
stats = require 'stats-lite'

GmailAuth = require './GmailAuth'
Domain = require './Domain'

schema = new mongoose.Schema
  to_email: String
  from_domain: String
  year: Number
  week: Number
  count: Number
  first_use: Boolean

schema.index
  year: -1
  week: -1

schema.statics.insert = (doc, cb) ->
  ins = new WeekDomainCount doc
  ins.save cb

schema.statics.add = (email, domain, week, year, count, cb) ->
  WeekDomainCount.findOne
    to_email: email
    from_domain: domain
    week: week
    year: year
  , (err, doc) ->
    if doc?
      #console.log "WeekDomainCount for #{email}:#{domain}:#{week}:#{year} updated to #{count}"
      doc.count = doc.count + count
      doc.save cb
    else
      #console.log "WeekDomainCount for #{email}:#{domain}:#{week}:#{year} inserted with #{count}"
      WeekDomainCount.insert
        to_email: email
        from_domain: domain
        count: count
        week: week
        year: year
      , cb

schema.statics.getWeekTotals = (year, week, exclude, done) ->
  WeekDomainCount.sumWeeks year, [week], exclude, done

schema.statics.getYearTotals = (year, exclude, done) ->
  currentYear = moment().year()
  currentWeek = moment().week()
  yearEnd = if year is currentYear then currentWeek else 52
  WeekDomainCount.sumWeeks year, [1 .. yearEnd], exclude, done

schema.statics.getQuarterTotals = (year, quarter, exclude, done) ->
  quarterStart = 1 + 13 * (quarter - 1)
  quarterEnd = 13 * quarter
  currentYear = moment().year()
  currentWeek = moment().week()
  quarterEnd = currentWeek if year is currentYear and quarterEnd > currentWeek
  WeekDomainCount.sumWeeks year, [quarterStart .. quarterEnd], exclude, done

schema.statics.getMonthTotals = (year, month, exclude, done) ->
  monthStart = 1 + 4 * (month - 1)
  monthStart += 1 if monthStart > 1
  monthStart += 1 if monthStart > 10
  # todo - use more precise system to determine month start times in weeks
  currentYear = moment().year()
  currentWeek = moment().week()
  monthEnd = monthStart + 4
  monthEnd = currentWeek if year is currentYear and monthEnd > currentWeek
  WeekDomainCount.sumWeeks year, [monthStart .. monthEnd], exclude, done

schema.statics.sumWeeks = (year, weekRange, exclude, done) -> 
  GmailAuth.find().exec (err, users) ->
    totalUsers = if err? then 0 else users.length
    async.map weekRange
    , (week, cb) ->
      WeekDomainCount.getWeekStats year, week, (result) -> cb null, result
    , (err, results) ->
      sum = {}
      for week, result of results
        for item in result
          domain = item.domain.split('.').slice(-2).join '.'
          sum[domain] ?=
            users: []
          sum[domain].hits ?= 0
          sum[domain].newUsers ?= 0
          sum[domain].hits += item.hits
          sum[domain].newUsers += item.newUsers.length
          sum[domain].users.push user for user in item.users
      domainList = []
      for domain, obj of sum
        unique = _.uniq sum[domain].users
        if exclude?
          if exclude.substr(0, 1) is '!'
            continue unless exclude.substr(1) in unique
          else continue if exclude in unique
        obj.numUsers = unique.length
        domainList.push domain
      Domain.find
        domain:
          $in: domainList
      .exec (err, domains) ->
        domainInfo = {}
        for domain in domains
          domainInfo[domain.domain] =
            name: domain.organization
            logo: domain.logo
            desc: domain.description
          sum[domain.domain].earliest = domain.earliest
        ret = []
        for domain in domainList
          numUsers = sum[domain].numUsers
          ret.push
            domain: domain
            hits: sum[domain].hits
            organization: domainInfo[domain]?.name
            logo_url: domainInfo[domain]?.logo
            description: domainInfo[domain]?.desc
            usePercent: parseInt 100*numUsers/totalUsers
            earliest: sum[domain].earliest
            numUsers: numUsers
            newUsers: sum[domain].newUsers
        done ret

schema.statics.getUserDomains = (email, year, cb) ->
  match =
    $match:
      to_email: email
      year: parseInt year
  group =
    $group:
      _id: "$from_domain"
      count:
        $sum: "$count"
  sort =
    $sort:
      count: -1
  WeekDomainCount.aggregate [match, group, sort]
  .exec (err, result) ->
    #console.log "Domains for #{email}: " + JSON.stringify result, null, 2
    cb result

schema.statics.getDomainUsers = (domain, year, cb) ->
  match =
    $match:
      from_domain: domain
      year: parseInt year
  group =
    $group:
      _id: "$week"
      count:
        $sum: "$count"
      users:
        $push: "$to_email"
  sort =
    $sort:
      _id: 1
  #console.log "Aggregating: " + JSON.stringify [match, group, sort], null, 2
  WeekDomainCount.aggregate [match, group, sort]
  .exec (err, result) ->
    console.log "Error in getDomainUsers: ", err if err?
    allUsers = []
    total = 0
    data = result.map (item) ->
      allUsers.push user for user in item.users
      total += item.count
      week: item._id
      messages: item.count
      active_users: _.uniq(item.users).length
      total_users: _.uniq(allUsers).length
    cb data

schema.statics.getWeekStats = (year, week, cb) ->
  match = 
    $match:
      week: week
      year: year
  group =
    $group:
      _id: "$from_domain"
      count:
        $sum: "$count"
      newUsers:
        $push: "$first_use"
      numUsers:
        $sum: 1
      users:
        $push: "$to_email"
  WeekDomainCount.aggregate [match, group]
  .exec (err, result) ->
    cb _.map result, (item) ->
      domain: item._id
      hits: item.count
      numUsers: item.numUsers
      users: item.users
      newUsers: item.newUsers

schema.statics.getWeeklyNewUsers = (domain, cb) ->
  WeekDomainCount.find
    from_domain: domain
  .sort
    year: 1
    week: 1
  .exec (err, results) ->
    firstUse = {}
    for result in results
      continue if firstUse[result.to_email]?
      firstUse[result.to_email] =
        year: result.year
        week: result.week
    cb firstUse

module.exports = WeekDomainCount = mongoose.model 'WeekDomainCount', schema  