GmailAPI = require('./GmailApi').GmailAPI
HistoryItem = require './models/HistoryItem'
ResponseTime = require './models/ResponseTime'
Thread = require './models/Thread'

elapsed = (diff) ->
  Math.floor(moment.duration(diff).asHours()) + moment.utc(diff).format(":mm:ss")

stripName = (person) ->
  start = person.indexOf '<'
  return person.toLowerCase() if start is -1
  email = person.substr start + 1
  return (email.slice 0, -1).toLowerCase()

getHeader = (headers, field) ->
  _.result _.find(headers, (header) -> header.name.toLowerCase() is field.toLowerCase()), 'value'

exports.MailRate = class MailRate extends GmailAPI

  finished: () ->
    switch @mode
      when 'response_time'
        HistoryItem.insert
          import_type: @mode
          email: @emailAddress
          import_start: @importStart
          first_email: @earliestMessage
          last_email: @latestMessage
          total_stats_imported: @responseTimesFound
          threads_processed: @threadsProcessed
        , (ok) => console.log "#{@mode} HistoryItem insert OK" if ok
        console.log "Import finished for #{@emailAddress}"
        console.log "latest message #{@latestMessage.format 'MM/DD/YYYY'}" if @latestMessage?
        console.log "imported #{@responseTimesFound} response times"
        for person, times of @MRT
          #console.log "Times for person #{person}:" + JSON.stringify times, null, 2
          email = stripName person
          result = "Mean response time for #{email}=" + elapsed(stats.mean(times))
          if times.length > 1000  # This should exclude chats, since "-in:chats" filter doesn't seem to work.
            result += ", median=" + elapsed(stats.median(times))
            result += " (std dev " + elapsed(stats.stdev(times))
            result += ", samp size " + times.length + ")"
          console.log result
      when 'response_rate'
        HistoryItem.insert
          import_type: @mode
          email: @emailAddress
          target: @responseTarget
          import_start: @importStart
          first_email: @earliestMessage
          last_email: @latestMessage
          total_stats_imported: @responseRate.hits + @responseRate.misses
          threads_processed: @threadsProcessed
        , (ok) => console.log "#{@mode} HistoryItem insert OK" if ok
    super()

  processThread: (threadInfo, threadId, messages) ->
    switch @mode
      when 'response_time'
        Thread.insertThread threadInfo, (insertOK) -> console.log "Insert thread failed" unless insertOK
        @findResponseTimes threadId, messages
      when 'response_rate' then @findResponseRates messages

  handleUnchangedThread: (thread) ->
    if @mode is 'response_rate'
      ResponseTime.find
        thread_id: thread.thread_id
      , (err, responses) =>
        #console.log JSON.stringify responses
        if responses.length
          console.log "Found response in saved thread"
          ++@responseRate.hits # ToDo - make sure these are actual responses, not multiple emails from sender
        else
          console.log "No responses in saved thread"
          ++@responseRate.misses
        console.log "Running RR =>" + (@responseRate.hits / (@responseRate.hits + @responseRate.misses))
        @handleNextThread()
    else @handleNextThread()

  import: () ->
    @initImport()
    @mode = 'response_time'
    @MRT = {}
    @responseTimesFound = 0
    params =
      q: "-in:chats -category:promotions -category:social -category:updates"
    HistoryItem.getLatestImport @emailAddress, (lastEmailDate) =>
      if lastEmailDate
        console.log "*** [#{@emailAddress}] DETECTED PREVIOUS IMPORT *** importing emails after #{lastEmailDate}"
        params.q += ' after:' + moment(lastEmailDate).startOf('day').format 'YYYY/MM/DD'
      else
        console.log "**** [#{@emailAddress}] NO IMPORT HISTORY *** importing all emails"
      @fetchThreads params

  getResponseRate: (user, target, startDate, cb) ->
    @totalEntries = 0
    @threadsProcessed = 0
    @mode = 'response_rate'
    @responseTarget = target
    @responseRate =
      hits: 0
      misses: 0
    @onFinished = () => cb @responseRate
    console.log "Fetching threads w/ query => from:#{user} to:#{target}"
    query = "from:#{user} to:#{target}"
    if startDate
      query += " after:" + moment(startDate).startOf('day').format 'YYYY/MM/DD'
    @fetchThreads
      q: query

  findResponseRates: (messages) ->
    console.log "FINDING RESPONSE RATES FOR #{@responseTarget}"
    hasResponse = false
    for message in messages
      toHeader = getHeader message.payload.headers, 'to'
      fromHeader = getHeader message.payload.headers, 'from'
      listName = getHeader message.payload.headers, 'list-id'
      if listName?
        console.log "Skipping list #{listName}"
        continue
      unless toHeader? and fromHeader?
        console.log "Missing To or From header"
        console.log JSON.stringify message.payload.headers, null, 2
        continue
      to = (_.map toHeader.split(/, */), (name) -> stripName name).join '/'
      from = stripName fromHeader
      console.log "To: #{to} From: #{from}"
      @updateDateRange moment getHeader message.payload.headers, 'date'
      if from is @responseTarget
        console.log "Found response in new thread"
        ++@responseRate.hits
        hasResponse = true
        break
    unless hasResponse
      console.log "No responses in new thread"
      ++@responseRate.misses
    console.log "Running RR =>" + (@responseRate.hits / (@responseRate.hits + @responseRate.misses))

  findResponseTimes: (threadId, messages) ->
    return unless messages.length > 1
    headers = {}
    threadMessages = []
    index = 0
    isDirectResponse = false
    
    for message in messages
      #console.log JSON.stringify message, null, 2

      if index > 0
        unless lastSender?
          oldDate = parsedDate
        else if lastSender isnt currentSender
          oldDate = parsedDate  
        lastSender = currentSender
        #console.log "Previous email:\n" + JSON.stringify entry, null, 2
        #if entry.from.indexOf(@emailAddress) isnt -1
        #  isDirectResponse = true

      unless message.payload?
        console.log "No payload for message:", message
        continue

      for header in message.payload.headers
        headerName = header.name.toLowerCase()
        headers[headerName] = header.value if headerName in @captureHeaders

      threadMessages.push { headers: headers }
      entry = headers
      entry.threadId = threadId
      entry.threadCount = messages.length
      entry["moment"] = parsedDate = moment entry["date"]  
      currentSender = stripName headers.from

      if index is 0
        console.log "Thread initiator is #{currentSender}"
        threadStarter = currentSender
      else
        if threadStarter is currentSender
          console.log "#{index+1} message from initiator"
        else
          console.log "#{index+1} message from #{currentSender}"
        if lastSender isnt currentSender
          console.log "===== ===== Time for #{currentSender} to reply to #{lastSender}: " + parsedDate.diff oldDate

      doc = undefined
      #if isDirectResponse is true and entry.from.indexOf(@emailAddress) is -1

      if index > 0 and lastSender isnt currentSender
        delay = parsedDate.diff oldDate
        if delay > 1
          #console.log "Time for #{entry.from} to respond: " + delay
          doc =
            thread_id: message.threadId
            message_id: message.id
            email_to: @emailAddress
            sent_at: oldDate.toDate()
            email_from: currentSender
            received_at: parsedDate.toDate()
            response_time: delay
          if entry.from of @MRT
            @MRT[currentSender].push delay
          else
            @MRT[currentSender] = [delay]

      #  isDirectResponse = false

      entry["date"] = parsedDate.format "MM/DD/YYYY"
      entry["time"] = parsedDate.format "HH:mm:ss"
      if entry["list-id"]
        console.log "Skipping thread #{threadId} (mailing list)"
        continue
      else
        #@writer.write entry
        if doc?
          ResponseTime.insertStats doc, (insertOk) ->
            console.log "Insert failure" unless insertOk
          @updateDateRange parsedDate
          ++@responseTimesFound 
        #console.log "Total entries written: " + ++@totalEntries
        index++
      headers = {}
