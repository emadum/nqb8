async = require 'async'
_ = require 'lodash'

ResponseRate = require './models/ResponseRate'
ResponseTime = require './models/ResponseTime'

exports.MailStats =
class MailStats
	@getAggregateStats: (user, email, gmailApi, done) ->
	    console.log "Getting aggregate stats: email=#{email} user=#{user}"
	    user = '' unless user?
	    user = user.toLowerCase()
	    email = email.toLowerCase()
	    console.log "Running parallel queries"
	    async.parallel [
	         (cb) -> ResponseTime.getMRT email, cb
	        ,(cb) -> ResponseRate.getRR user, email, gmailApi, cb
	    ], (err, results) -> done _.assign.apply _, results