GmailAPI = require('./GmailApi').GmailAPI
MailStats = require('./MailStats').MailStats
GmailAuth = require './models/GmailAuth'
HistoryItem = require './models/HistoryItem'
TrackedMessage = require './models/TrackedMessage'
TrackedLink = require './models/TrackedLink'

_ = require 'lodash'
async = require 'async'
useragent = require 'useragent'
moment = require 'moment-timezone'

currentVersion = '0.1.15';

findClicks = (query, cb) ->
  TrackedLink.find().exists "clicks.clicked_at"
  .exec (err, links) ->
    messageIds = links.map (link) -> link.message
    #console.log "MESSAGES => " + messageIds.join ','
    query._id = 
      $in: messageIds
    TrackedMessage.find query
    .exec (err, messages) ->
      messageLookup = new Object
      messageLookup[message._id] = message for message in messages
      clicks = new Array
      for link in links
        message = messageLookup[link.message]
        continue unless message?
        #console.log "MESSAGE TO =>" + JSON.stringify message.to, null, 2
        reducer = (all, cur) -> all + "<div>" + cur.replace(new RegExp(" ?<"), "&nbsp;&lt;") + "&gt;</div>"
        messageTo = _.reduce message.to, reducer, ""
        #console.log "CONSOLIDATED => " + messageTo
        for click in link.clicks
          clicks.push
            #date_sent: moment(message.sent_on).tz('America/Los_Angeles').format 'MM-DD-YYYY h:mm:ss a'
            date_sent: message.sent_on
            sender: message.sender
            subject: message.subject
            to: messageTo
            clicked_at: moment(click.clicked_at).tz('America/Los_Angeles').format 'MM-DD-YYYY h:mm:ss a'
            clicked_moment: click.clicked_at
            ip: click.ip
            link: if /^https?:/.test link.link then link.link else "http://#{link.link}"
            label: if link.label then link.label else link.link
      cb clicks.sort (a, b) ->
          if a.clicked_moment > b.clicked_moment
            return -1
          else if a.clicked_moment < b.clicked_moment
            return 1
          else return 0

module.exports =
  index: (req, res) ->
    params =
      page_title: "Install MailRate"
      current_version: currentVersion
    params.email = req.session.email if req.session.email?
    params.dashboardUser = req.session.dashboardUser if req.session.dashboardUser?
    res.render 'install', params

  import: (req, res) ->
    console.log "Requesting authorization for import"
    gmailApi = new GmailAPI 'me'
    req.session.on_auth_ok = 'import'
    res.redirect gmailApi.getAuthUrl()

  clicks_json: (req, res) ->
    if req.params.email?
      findClicks
        sender: req.params.email
      , (clicks) ->
        res.json clicks
    else
      res.json []

  csv: (req, res) ->
    switch req.params.dataset
      when "linksClicked"
        findClicks
          sender: req.params.email
        , (clicks) ->
          res.setHeader 'Content-disposition', "attachment; filename=#{req.params.email}-clicks.csv"
          res.csv clicks

  dashboard: (req, res) ->
    currentAuth = req.session.dashboardUser
    if currentAuth? and not req.params.email?
      res.redirect "/dashboard/#{currentAuth}"
      return
    unless currentAuth? and req.params.email? and req.params.email is currentAuth
      req.session.on_auth_ok = 'dashboard'
      res.redirect '/login'
      return
    res.render 'clicks',
      page_title: MailRate - User Dashboard
      section: 'Link Tracking'
      title: 'Clicks'
      layout: 'lightblue'
      admin_mode: false
      email: req.params.email

  auth: (req, res) ->
    gmailApi = new GmailAPI 'me'
    unless req.query.error?
      gmailApi.monitorQuota()
      console.log "Confirming authorization..."
      gmailApi.getAccessToken req.query.code, ->
        gmailApi.fetchUser (email) ->
          console.log "Authorized: #{email}"
          if 'email' in req.session
            req.session.email.push email
          else
            req.session.email = [email]
          console.log "AUTH OK => #{email} #{req.session.on_auth_ok}"
          switch req.session.on_auth_ok
            when 'import'
              gmailApi.import()
              res.redirect '/'
            when 'dashboard'
              req.session.dashboardUser = email
              res.redirect "/dashboard/#{email}"
            else
              if req.session.on_auth_ok.indexOf '/admin' is 0
                console.log "Confirming admin user..."
                GmailAuth.isAdmin email
                , () ->
                  console.log "Admin user #{email} confirmed"
                  req.session.adminUser = email
                  res.redirect req.session.on_auth_ok
                , () ->
                  console.log "User #{email} not an admin"
                  res.status(301).end()

    else
      req.session.error = "Authorization failed: #{req.query.error}<br/><a href='/import'>Try again</a>"
      res.redirect '/'

  login: (req, res) ->
    gmailApi = new GmailAPI 'me'
    res.redirect gmailApi.getAuthUrl()

  logout: (req, res) ->
    delete req.session.dashboardUser if req.session.dashboardUser?
    res.redirect '/'

  adminLogout: (req, res) ->
    delete req.session.adminUser
    res.redirect '/'

  ltAdmin: (req, res) ->
    #unless req.query.pw? and req.query.pw is "nqb8eyesonly"
    #  res.status(301).end()
    #  return
    findClicks {}, (clicks) ->
      res.render 'ltAdmin',
        page_title: 'MailRate Admin | Link Tracking'
        clicks: clicks
        layout: 'lightblue'
        admin_mode: true

  admin: (req, res) ->
    console.log "Preparing admin page..."
    GmailAuth.find().exec (err, result) ->
      async.map result, (user, cb) ->
        params =
          email: user.email
          auth_status: if user.refresh_token? then 'OK' else 'INVALID'
        HistoryItem.find
          email: user.email
        .sort
          import_finish: -1
        .exec (err1, historyItems) ->
          #console.log JSON.stringify historyItems, null, 2
          unless historyItems.length
            params.last_import = 'Never'
          else
            params.last_import = historyItems[0].import_finish
          cb null, params
      , (err2, results) ->
        console.log "Done preparing admin page!"
        res.render 'admin',
          page_title: "MailRate Admin"
          users: results
          layout: 'lightblue'
          admin_mode: true

  track: (req, res) ->
    agent = useragent.parse req.headers['user-agent']
    TrackedLink.clicked req.params.trackingId, req.headers['x-real-ip'], agent.toString()
    #console.log "Redirect => #{req.params.protocol}, #{req.params.url}, #{req.originalUrl}"
    matches = req.originalUrl.match /\/track\/[\w-]+\/(.+)$/
    if matches?
        #console.log "Matched URL #{matches[1]}"
        res.redirect matches[1].split(/:\//).join '://'
    else
        res.redirect req.params.protocol + '//' + req.params.url

  api: (req, res) ->
    method = req.params.method    
    user = req.query.user
    email = req.params.email
    console.log "Calling API method #{method} from #{user} for #{email}"
    gmailApi = new GmailAPI 'me'
    params =
      success: false
      api_method: method
    switch method
      when "recentClicks"
        findClicks
          sender: user
        , (clicks) ->
          res.json clicks
      when "trackLinks"
        if req.query.links?
          message =
            sender: user
            subject: req.query.subject
            to: _.map JSON.parse(req.query.to), (i) -> i.email
            cc: _.map JSON.parse(req.query.cc), (i) -> i.email
            bcc: _.map JSON.parse(req.query.bcc), (i) -> i.email
            links: JSON.parse req.query.links
            textLinks: JSON.parse req.query.textLinks
          console.log "Tracking #{message.links.length} links..."
          console.log "(Subject) #{message.subject} (To) #{message.to}"
          TrackedMessage.track message, (err, trackedLinks) -> res.json trackedLinks
        else res.json
          error: "No links"
      when "incrementalImport"
        console.log "Incremental import of threads: " + req.query.threadIds
      when "stats"
        MailStats.getAggregateStats user, email, gmailApi, (result) ->
          params.success = true
          params = _.assign params, result
          res.json params
      when "auth"
        console.log "Authing user #{req.params.email}"
        response =
          email: req.params.email
          status: false
        gmailApi.loadSavedAuth req.params.email, (ok) ->
          console.log "Auth for #{req.params.email}=#{ok}"
          response.status = true if ok
          res.json response
      else res.json params

