# NQB8 -- MailRate MVP
# Author: Eric Adum (eric@nqb8.com)

### ToDo List
  - calculate response rate
###

# 3rd Party Modules
fs = require 'fs'
os = require 'os'
moment = require 'moment'
csvWriter = require 'csv-write-stream'
stats = require 'stats-lite'
_ = require 'lodash'

# Models
ResponseTime = require './models/ResponseTime'
Thread = require './models/Thread'
GmailAuth = require './models/GmailAuth'
HistoryItem = require './models/HistoryItem'

# Gmail API
google = require 'googleapis'
OAuth2Client = google.auth.OAuth2
gmail = google.gmail 'v1'

# Client ID and client secret are available at
# https://code.google.com/apis/console
CLIENT_ID = '1098310743763-1okt2a9nsmodh6na91cmbnaeeo5b6ddb.apps.googleusercontent.com'
CLIENT_SECRET = 'O6H05SpCaAamYgEiGAMOIk90'
REDIRECT_URL = "http://#{if os.hostname() is 'ayyus' then 'dev.' else ''}mailrate.com/auth"

elapsed = (diff) ->
  Math.floor(moment.duration(diff).asHours()) + moment.utc(diff).format(":mm:ss")

stripName = (person) ->
  start = person.indexOf '<'
  return person.toLowerCase() if start is -1
  email = person.substr start + 1
  return (email.slice 0, -1).toLowerCase()

getHeader = (headers, field) ->
  _.result _.find(headers, (header) -> header.name.toLowerCase() is field.toLowerCase()), 'value'

exports.GmailAPI =
class GmailAPI
  captureHeaders: ['delivered-to', 'from', 'to', 'date', 'list-id']
  #maxEntries: 500

  constructor: (@userId) ->
    # GMail API
    console.log "Initialized GmailAPI client for user '#{@userId}', authorizing..."
    @oauth2Client = new OAuth2Client CLIENT_ID, CLIENT_SECRET, REDIRECT_URL
    
    # CSV output
    #@writer = csvWriter {headers: ['threadId', 'threadCount', 'from', 'to', 'delivered-to', 'date', 'time']}
    #@writer.pipe fs.createWriteStream 'emails.csv'

    return

  monitorQuota: () ->
    @totalQuota = 0
    @quotaPerSecond = 0
    @rateMonitorInterval = setInterval =>
      console.log "=====> [#{@emailAddress}] Calls this second: #{@quotaPerSecond} <=====" if @quotaPerSecond
      @quotaPerSecond = 0
    , 1000

  quotaUsed: (amount) ->
    @quotaPerSecond += amount
    @totalQuota += amount

  loadSavedAuth: (email, cb) ->
    #console.log "Loading saved auth for #{email}"
    GmailAuth.find
      email: email
    , (err, result) =>
      #console.log JSON.stringify result, null, 2
      return cb false if err? or not result.length
      @refreshAccess result[0], cb

  refreshAccess: (saved, cb) ->
    @oauth2Client.setCredentials
        access_token: saved.access_token
        refresh_token: saved.refresh_token
    @oauth2Client.refreshAccessToken (err, tokens) =>
      console.log "Auth error: #{err}" if err?
      return cb false if err? or not tokens?
      #console.log "Refreshed access for saved auth", saved
      @oauth2Client.setCredentials tokens
      updated = _.assign saved, tokens
      updated.save (err) ->
        console.log "Error saving updated creds to mongo" if err
      cb true

  getAuthUrl: ->
    return @oauth2Client.generateAuthUrl
      access_type: 'offline'
      scope: 'https://www.googleapis.com/auth/gmail.readonly'
#      approval_prompt: 'force'
    
  getAccessToken: (code, cb) ->
    # request access token
    @oauth2Client.getToken code, (err, tokens) =>
      # set tokens to the client
      # TODO: tokens should be set by OAuth2 client.
      console.log tokens
      if tokens?
        if "expiry_date" in tokens
          console.log "Access expires #{moment(tokens.expiry_date).fromNow()}"
        @authTokens = tokens
        @oauth2Client.setCredentials tokens
        cb true
      else
        cb false
      return
    return

  fetchUser: (cb) ->
    #console.log "Hitting API: gmail.users.getProfile"
    queryParams =
      userId: 'me'
      auth: @oauth2Client
    gmail.users.getProfile queryParams, (err, results) =>
      if err
        console.log 'An error occured (fetchUser)', err
        return
      @quotaUsed 1
      @emailAddress = results.emailAddress
      console.log "Logged in as #{@emailAddress}"
      if @authTokens?
        params = _.assign @authTokens,
          email: @emailAddress
        delete @authTokens.token_type
        console.log "Saving auth", JSON.stringify params, null, 2
        GmailAuth.save params, (insertOK) ->
          console.log "Failed to save auth token to Mongo" unless insertOK  
      cb @emailAddress

  fetchThreads: (params) ->
    console.log "fetchThreads called in #{@mode} mode"
    #console.log "Hitting API: gmail.users.threads.list"
    queryParams =
      userId: @userId
      auth: @oauth2Client
      q: ""
    if params?
      @savedParams = params
      queryParams = _.assign queryParams, params
    queryParams.q += " -in:chats -category:promotions -category:social -category:updates"
    queryParams.pageToken = @nextPageToken if @nextPageToken?
    gmail.users.threads.list queryParams, (err, results) =>
      if err
        console.log 'An error occured (fetchThreads) on page #{@nextPageToken}', err
        @finished()
        return
      @quotaUsed 10
      if results and results.threads
        #console.log results
        console.log "------------------------------------------------------"
        console.log "[#{@emailAddress}] QUERY => #{queryParams.q}"
        console.log "Fetched #{results.threads.length} threads on page #{@nextPageToken}"
        console.log "Next page token: #{results.nextPageToken}"
        console.log "Estimated total results: #{results.resultSizeEstimate}"
        console.log "------------------------------------------------------"
        @threads = results.threads
        @nextPageToken = results.nextPageToken
        @handleNextThread()
      else
        console.log "No results for query" + JSON.stringify queryParams, null, 2
        @finished()

  handleNextThread: ->
    if @threads.length
      # Check if we already have this thread in Mongo
      nextThread = @threads.shift()
      ++@threadsProcessed
      Thread.checkForNewMessages @emailAddress, nextThread, (result, thread) =>
        if result is true
          console.log "Thread doesn't exist or was updated: #{nextThread.id}"
          @fetchThread nextThread, 3
        else
          console.log "No new messages in thread #{nextThread.id} (mode=#{@mode})"
          if @mode is 'response_rate'
            ResponseTime.find
              thread_id: thread.thread_id
            , (err, responses) =>
              #console.log JSON.stringify responses
              if responses.length
                console.log "Found response in saved thread"
                ++@responseRate.hits # ToDo - make sure these are actual responses, not multiple emails from sender
              else
                console.log "No responses in saved thread"
                ++@responseRate.misses
              console.log "Running RR =>" + (@responseRate.hits / (@responseRate.hits + @responseRate.misses))
              @handleNextThread()
          else @handleNextThread()
    else
      console.log "Done, next page? #{@nextPageToken}"
      if @maxEntries > 0 and @totalEntries >= @maxEntries
        console.log "Reached max entries #{@totalEntries}/#{@maxEntries}"
        @finished()
        return
      if @nextPageToken?
        if @savedParams? then @fetchThreads @savedParams else @fetchThreads()
      else
        @finished()

  finished: ->
    console.log "----- Done with #{@mode} ------"    
    clearInterval @rateMonitorInterval
    console.log "Total quota used: #{@totalQuota}"
    delete @savedParams
    #@writer.end()
    #console.log JSON.stringify @MRT, null, 2
    switch @mode
      when 'response_time'
        HistoryItem.insert
          import_type: @mode
          email: @emailAddress
          import_start: @importStart
          first_email: @earliestMessage
          last_email: @latestMessage
          total_stats_imported: @responseTimesFound
          threads_processed: @threadsProcessed
        , (ok) => console.log "#{@mode} HistoryItem insert OK" if ok
        console.log "Import finished for #{@emailAddress}"
        console.log "latest message #{@latestMessage.format 'MM/DD/YYYY'}" if @latestMessage?
        console.log "imported #{@responseTimesFound} response times"
        for person, times of @MRT
          #console.log "Times for person #{person}:" + JSON.stringify times, null, 2
          email = stripName person
          result = "Mean response time for #{email}=" + elapsed(stats.mean(times))
          if times.length > 1000  # This should exclude chats, since "-in:chats" filter doesn't seem to work.
            result += ", median=" + elapsed(stats.median(times))
            result += " (std dev " + elapsed(stats.stdev(times))
            result += ", samp size " + times.length + ")"
          console.log result
      when 'response_rate'
        HistoryItem.insert
          import_type: @mode
          email: @emailAddress
          target: @responseTarget
          import_start: @importStart
          first_email: @earliestMessage
          last_email: @latestMessage
          total_stats_imported: @responseRate.hits + @responseRate.misses
          threads_processed: @threadsProcessed
        , (ok) => console.log "#{@mode} HistoryItem insert OK" if ok
    @onFinished() if @onFinished?
    
                  
  fetchThread: (thread, retry) ->
    #console.log 'Fetching thread ' + thread.id 
    #console.log "Hitting API: gmail.users.threads.get"
    queryParams =
      userId: @userId
      id: thread.id
      auth: @oauth2Client
      format: 'metadata'
    gmail.users.threads.get queryParams, (err, result) =>
      if err
        console.log 'An error occured (fetchThread)', err
        console.log "Retries remaining: #{retry}"
        if retry > 0
          @fetchThread thread, retry-1
        else
          @finished()
        return
      @quotaUsed 10
      #console.log "#{result.messages.length} messages in thread #{thread.id}"
      threadInfo =
        user: @emailAddress
        thread_id: thread.id
        thread_count: result.messages.length
        history_id: thread.historyId
      #console.log "CURRENT MODE => #{@mode}"
      switch @mode
        when 'response_time'
          Thread.insertThread threadInfo, (insertOK) -> console.log "Insert thread failed" unless insertOK
          @findResponseTimes thread.id, result.messages
        when 'response_rate' then @findResponseRates result.messages
      @handleNextThread()
      return
    return

  import: () ->
    @totalEntries = 0
    @threadsProcessed = 0
    @mode = 'response_time'
    @MRT = {}
    @responseTimesFound = 0
    @importStart = new Date()
    HistoryItem.getLatestImport @emailAddress, (lastEmailDate) =>
      if lastEmailDate
        console.log "*** [#{@emailAddress}] DETECTED PREVIOUS IMPORT *** importing emails after #{lastEmailDate}"
        @fetchThreads
          q: 'after:' + moment(lastEmailDate).startOf('day').format 'YYYY/MM/DD'
      else
        console.log "**** [#{@emailAddress}] NO IMPORT HISTORY *** importing all emails"
        @fetchThreads()

  getResponseRate: (user, target, startDate, cb) ->
    @totalEntries = 0
    @threadsProcessed = 0
    @mode = 'response_rate'
    @responseTarget = target
    @responseRate =
      hits: 0
      misses: 0
    @onFinished = () => cb @responseRate
    console.log "Fetching threads w/ query => from:#{user} to:#{target}"
    query = "from:#{user} to:#{target}"
    if startDate
      query += " after:" + moment(startDate).startOf('day').format 'YYYY/MM/DD'
    @fetchThreads
      q: query

  findResponseRates: (messages) ->
    console.log "FINDING RESPONSE RATES FOR #{@responseTarget}"
    hasResponse = false
    for message in messages
      toHeader = getHeader message.payload.headers, 'to'
      fromHeader = getHeader message.payload.headers, 'from'
      listName = getHeader message.payload.headers, 'list-id'
      if listName?
        console.log "Skipping list #{listName}"
        continue
      unless toHeader? and fromHeader?
        console.log "Missing To or From header"
        console.log JSON.stringify message.payload.headers, null, 2
        continue
      to = (_.map toHeader.split(/, */), (name) -> stripName name).join '/'
      from = stripName fromHeader
      console.log "To: #{to} From: #{from}"
      @updateDateRange moment getHeader message.payload.headers, 'date'
      if from is @responseTarget
        console.log "Found response in new thread"
        ++@responseRate.hits
        hasResponse = true
        break
    unless hasResponse
      console.log "No responses in new thread"
      ++@responseRate.misses
    console.log "Running RR =>" + (@responseRate.hits / (@responseRate.hits + @responseRate.misses))

  findResponseTimes: (threadId, messages) ->
    return unless messages.length > 1
    headers = {}
    threadMessages = []
    index = 0
    isDirectResponse = false
    
    for message in messages
      #console.log JSON.stringify message, null, 2

      if index > 0
        unless lastSender?
          oldDate = parsedDate
        else if lastSender isnt currentSender
          oldDate = parsedDate  
        lastSender = currentSender
        #console.log "Previous email:\n" + JSON.stringify entry, null, 2
        #if entry.from.indexOf(@emailAddress) isnt -1
        #  isDirectResponse = true

      unless message.payload?
        console.log "No payload for message:", message
        continue

      for header in message.payload.headers
        headerName = header.name.toLowerCase()
        headers[headerName] = header.value if headerName in @captureHeaders

      threadMessages.push { headers: headers }
      entry = headers
      entry.threadId = threadId
      entry.threadCount = messages.length
      entry["moment"] = parsedDate = moment entry["date"]  
      currentSender = stripName headers.from

      if index is 0
        console.log "Thread initiator is #{currentSender}"
        threadStarter = currentSender
      else
        if threadStarter is currentSender
          console.log "#{index+1} message from initiator"
        else
          console.log "#{index+1} message from #{currentSender}"
        if lastSender isnt currentSender
          console.log "===== ===== Time for #{currentSender} to reply to #{lastSender}: " + parsedDate.diff oldDate

      doc = undefined
      #if isDirectResponse is true and entry.from.indexOf(@emailAddress) is -1

      if index > 0 and lastSender isnt currentSender
        delay = parsedDate.diff oldDate
        if delay > 1
          #console.log "Time for #{entry.from} to respond: " + delay
          doc =
            thread_id: message.threadId
            message_id: message.id
            email_to: @emailAddress
            sent_at: oldDate.toDate()
            email_from: currentSender
            received_at: parsedDate.toDate()
            response_time: delay
          if entry.from of @MRT
            @MRT[currentSender].push delay
          else
            @MRT[currentSender] = [delay]

      #  isDirectResponse = false

      entry["date"] = parsedDate.format "MM/DD/YYYY"
      entry["time"] = parsedDate.format "HH:mm:ss"
      if entry["list-id"]
        console.log "Skipping thread #{threadId} (mailing list)"
        continue
      else
        #@writer.write entry
        if doc?
          ResponseTime.insertStats doc, (insertOk) ->
            console.log "Insert failure" unless insertOk
          @updateDateRange parsedDate
          ++@responseTimesFound 
        #console.log "Total entries written: " + ++@totalEntries
        index++
      headers = {}

  updateDateRange: (date) ->
    if @latestMessage?      
      @latestMessage = date if date.isAfter @latestMessage
    else
      @latestMessage = date
    if @earliestMessage?
      @earliestMessage = date if date.isBefore @earliestMessage
    else
      @earliestMessage = date            

  fetchMessages: ->
    #console.log "Hitting API: gmail.users.messages.list"
    gmail.users.messages.list {
      userId: @userId
      auth: @oauth2Client
    }, (err, results) =>
      if err
        console.log 'An error occured (fetchMessages)', err
        return
      @quotaUsed 5
      if results and results.messages
        #console.log 'Fetched ' + results.messages.length + ' emails'
        @handleMessages results.messages
      return

  handleMessages: (messages) ->
    first = messages.shift()
    #console.log 'ID of first email is ' + first.id
    @fetchEmail first.id
    return

  fetchEmail: (messageId) ->
    #console.log 'Fetching message ' + messageId
    #console.log "Hitting API: gmail.users.messages.get"
    gmail.users.messages.get {
      userId: @userId
      id: messageId
      auth: @oauth2Client
      format: 'metadata'
    }, (err, result) ->
      if err
        console.log 'An error occured', err
        return
      @quotaUsed 5
      console.log JSON.stringify result, null, 4
      return
    return
