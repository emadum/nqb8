mongoose = require 'mongoose'
mongoose.connect 'mongodb://localhost/mailrate', (err) ->
	console.log "Error connecting to Mongo: ", err if err?
GmailAuth = require '../models/GmailAuth'
GmailAPI = require('../GmailApi').GmailAPI
gmailApi = new GmailAPI 'me'

importUser = (user) ->
  gmailApi.loadSavedAuth user, (ok) ->
    if ok is true
      gmailApi.fetchUser (email) ->
        if user is email
          console.log "Importing for #{email}..."
          gmailApi.import()
        else
          console.log "Auth issues; attempted login with #{user} but got #{email}"
    else
      console.log "Auth failed"

processNext = (result) ->
  if result.length then importUser result.shift().email else process.exit 0

importAllUsers = () ->
  console.log "Importing all  users..."
  
  GmailAuth.find().exec (err, result) ->
   
    console.log "Found #{result.length} users"
    if err?
      console.log "Error importing for all users", err
      return process.exit 1
    gmailApi.onFinished = () ->
      console.log "Import of #{gmailApi.emailAddress} finished"
      processNext result
    processNext result

user = process.argv[2]
if user is 'all' then importAllUsers() else importUser user