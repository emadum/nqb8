mongoose = require 'mongoose'

schema = new mongoose.Schema
    key: String
    locked_at: Date

schema.statics.lock = (key, result) ->
	lock = new Lock
		key: key
		locked_at: new Date()
	lock.save (err) ->
		if err?
			console.log "Error creating lock #{key}"
			result false, key
		else
			result true, key

schema.statics.unlock = (key, result) ->
	Lock.remove
		key: key
	, (err) ->
		if err?
			console.log "Error removing lock #{key}"
			result false, key
		else
			result true, key

schema.statics.locked = (key, ok) ->
	Lock.find
		key: key
	, (err, result) ->
		if err? or not result.length
			ok false, key
		else
			ok true, key

module.exports = Lock = mongoose.model 'Lock', schema