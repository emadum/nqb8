mongoose = require 'mongoose'
shortid = require 'shortid'

schema = new mongoose.Schema
  message:
    type: mongoose.Schema.Types.ObjectId
    ref: 'TrackedMessage'
  link: String
  label: String
  position: Number
  shortId:
    type: 'String'
    unique: true
    default: shortid.generate
  clicks: [
    ip: String
    browser: String
    clicked_at: Date
  ]

schema.statics.track = (params, cb) ->
    doc = new TrackedLink params
    doc.save (err, saved) -> cb err, saved

schema.statics.clicked = (linkShortId, ip, browser) ->
  TrackedLink.findOne
    shortId: linkShortId
  .exec (err, trackedLink) ->
    trackedLink.clicks.push
      ip: ip
      browser: browser
      clicked_at: new Date()
    trackedLink.save (err) ->
      if err?
        console.log "Error saving click on #{trackedLink.link} from #{ip}:", err
      else
        console.log "Saved click on #{trackedLink.link} from #{ip}"

module.exports = TrackedLink = mongoose.model 'TrackedLink', schema
