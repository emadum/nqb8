mongoose = require 'mongoose'
_ = require 'lodash'
async = require 'async'
ResponseTime = require './ResponseTime'
Lock = require './Lock'
moment = require 'moment'

schema = new mongoose.Schema
  email_to: String
  email_from: String
  responses: Number
  total: Number
  as_of: Date

cacheUserResponseRate = (from, to, responses, total, cb) ->
  doc = new ResponseRate
    email_from: from
    email_to: to
    responses: responses
    total: total
    as_of: new Date()
  doc.save (err) -> cb if err then false else true

getResponseRate = (email_to, email_from, startDate, gmailApi, cb) ->
  console.log "getResponseRate from #{email_from} to #{email_to} [START=#{startDate}]"
  gmailApi.loadSavedAuth email_to, (ok) ->
    if ok is true
      #console.log "getRR calling fetchThreads..."
      gmailApi.getResponseRate email_to, email_from, startDate, (responses) ->
        console.log "responses from #{email_from} to #{email_to} =>", responses
        cacheUserResponseRate email_from, email_to
        , responses.hits, responses.hits + responses.misses
        , (cacheOk) ->
          if not cacheOk then console.log "Caching failed"
        responses.user = email_to
        cb null, responses
    else
      console.log "Failed to authenticate #{email_to}"
      cacheUserResponseRate email_from, email_to, 0, 0, (cacheOk) ->
        if not cacheOk then console.log "Caching failed"
      cb null, null

getCached = (from, to, cb) ->
  ResponseRate.find
    email_from: from
    email_to: to
  .sort
    as_of: -1
  .exec (err, result) ->
    #console.log "GET CACHED =>", err, result
    if err? or not result.length
      return cb null
    ret =
      as_of: result[0].as_of
      email_from: from
      hits: 0
      misses: 0
    for cached in result
      ret.hits += cached.responses
      ret.misses += cached.total - cached.responses
    cb ret

sumResults = (results) ->
  hits = 0
  misses = 0
  for result in results
    hits += result.hits
    misses += result.misses
  return if hits then Math.round(100*hits/(hits+misses)) + '%' else 'N/A'

processResponseRates = (correspondents, email_to, startDates, gmailApi, done) ->
  Lock.locked "processRR_#{email_to}", (isLocked, key) ->
    if isLocked
      console.log "#{key} is locked (#{isLocked}), skipping"
      return
    Lock.lock "processRR_#{email_to}", (lockOK, key) ->
      console.log if lockOK then "Locked #{key}" else "Lock failed for #{key}"
    console.log "Correspondents:", correspondents
    queries = _.map correspondents, (correspondent) ->
      (cb) -> getResponseRate correspondent, email_to, startDates?[correspondent], gmailApi, cb
    async.series queries, (err, results) ->
      console.log "processResponseRates results", JSON.stringify results, null, 2
      done sumResults _.compact results
      Lock.unlock "processRR_#{email_to}", (unlockOK, key) ->
        console.log if unlockOK then "Unlocked #{key}" else "Unlock failed for #{key}"

schema.statics.getRR = (user, target, gmailApi, done) ->
  console.log "Finding response rates [u:#{user},t:#{target}]"
  ResponseTime.getCorrespondentsOf target, (err, correspondents) ->
    console.log "Correspondents=>" + JSON.stringify correspondents, null, 2
    if err?
      console.log "Error getting distinct targets in getRR: ", err
      done err, null
    else if not correspondents.length
      console.log "No distinct targets in getRR"
      done null,
        response_rate: 'N/A'
    else
      # Correspondents found, try to get cached results...
      expired = {}
      queries = _.map correspondents, (correspondent) ->
        (cb) -> getCached target, correspondent, (cached) ->
          console.log "Got cached entry for #{target}-#{correspondent}:", JSON.stringify cached, null, 2
          if cached?
            age = moment().diff moment(cached.as_of), 'days'
            if age > 1
              console.log "Cached entry for #{correspondent} expired #{age-1} days ago, updating"
              expired[correspondent] = cached.as_of
            cb null,
              user: cached.email_from
              hits: cached.hits
              misses: cached.misses
          else
            cb true 
      async.parallel queries, (err, results) ->
        if err?
          # Missing some cached results: run again and return processing=true status
          console.log "Missing cached results for #{target}"
          done null,
            processing: true
          processResponseRates correspondents, target, false, gmailApi, (result) ->
            console.log "Done processing correspondents for #{target}", result
        else
          # All results in cache
          # First, start an incremental update on any expired entries
          unless _.isEmpty expired
            console.log "Expired cache entries =>", JSON.stringify expired, null, 2
            processResponseRates _.keys(expired), target, expired, gmailApi, (result) ->
              console.log "Done processing expired RR cache items for #{target}", result
          # Return the cached values
          done null,
            response_rate: sumResults results
            processing: false

module.exports = ResponseRate = mongoose.model 'ResponseRate', schema
