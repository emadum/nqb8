mongoose = require 'mongoose'

schema = new mongoose.Schema
	email: String
	target: String
	import_type: String
	threads_processed: Number
	total_stats_imported: Number
	first_email: Date
	last_email: Date
	import_start: Date
	import_finish: Date

schema.statics.insert = (doc, result) ->
	doc.import_finish = new Date()
	historyItem = new HistoryItem doc	
	historyItem.save (err) ->
		if err?
			console.log "Error creating history item for #{email} =>" + JSON.stringify doc, null, 2
			result false
		else
			result true

schema.statics.getLatestImport = (email, cb) ->
  HistoryItem.find
    email: email
  .exists 'last_email'
  .sort
    import_finish: -1
  .exec (err, docs) ->
    cb if err? or not docs.length then false else docs[0].last_email

module.exports = HistoryItem = mongoose.model 'HistoryItem', schema
