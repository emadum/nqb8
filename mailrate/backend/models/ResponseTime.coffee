mongoose = require 'mongoose'
stats = require 'stats-lite'
moment = require 'moment'
_ = require 'lodash'
async = require 'async'

GmailAuth = require './GmailAuth'

elapsed = (diff) -> moment.duration(diff).humanize()

schema = new mongoose.Schema
    message_id: String
    email_to: String
    email_from: String
    sent_at: Date
    received_at: Date
    response_time: Number
    thread_id: String

schema.statics.getMRT = (email_from, cb) ->
    times = []
    threads = []
    firstTimes = []
    query = ResponseTime.find
        email_from: email_from
    .sort 'received_at'
    query.exec (err, docs) ->
        if err?
            cb err
            return
        for item in docs
            times.push item.response_time 
            if item.thread_id not in threads
                firstTimes.push item.response_time
                threads.push item.thread_id
        mrt = if times.length then elapsed stats.median times else 'N/A'
        mfrt = if firstTimes.length then elapsed stats.median firstTimes else 'N/A'
        result = 
            median_response_time: mrt
        #console.log "MRT for #{email_from} is #{elapsed stats.median times} based on #{times.length} samples" if times.length
        #console.log "MFRT for #{email_from} is #{elapsed stats.median firstTimes} based on #{firstTimes.length} samples" if firstTimes.length
        cb null, result

schema.statics.getCorrespondentsOf = (email, done) ->
    GmailAuth.authorizedEmails (currentUsers) ->
        async.parallel [
             (cb) ->
                ResponseTime.distinct 'email_from',
                    email_to: email
                .where 'email_from'
                .in currentUsers
                .exec cb
            ,(cb) ->
                ResponseTime.distinct 'email_to',
                    email_from: email
                .exec cb
        ], (err, results) ->
            if err?
                done err, null
            else
                correspondents = _.union results[0], results[1]
                done null, correspondents
        

schema.statics.insertStats = (doc, cb) ->
    doc.email_to = doc.email_to.toLowerCase() if doc.email_to?
    doc.email_from = doc.email_from.toLowerCase() if doc.email_from?
    ins = new ResponseTime doc
    ins.save (err) -> cb if err then false else true

schema.statics.getIndividualStats = (email_to, email_from, cb) ->
    email_to = email_to.toLowerCase()
    email_from = email_from.toLowerCase()
    query = ResponseTime.find
        email_to: email_to
        email_from: email_from
    query.exec (err, docs) ->
	   cb docs

module.exports = ResponseTime = mongoose.model 'ResponseTime', schema
