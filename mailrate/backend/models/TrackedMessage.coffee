mongoose = require 'mongoose'
async = require 'async'

TrackedLink = require './TrackedLink'

schema = new mongoose.Schema
  sender: String
  to: [String]
  cc: [String]
  bcc: [String]
  subject: String
  sent_on: Date

schema.statics.track = (params, done) ->
    params.sent_on = new Date()
    doc = new TrackedMessage params
    doc.save (err, saved) ->
      done err if err?
      async.parallel [
        (cb) -> trackLinks saved._id, params.links, cb
        (cb) -> trackLinks saved._id, params.textLinks, cb
      ], (err, result) ->
        done err if err?
        done null,
          links: result[0]
          textLinks: result[1]

trackLinks = (messageId, links, done) ->
  requests = links.map (link, pos) ->
    params =
      message: messageId
      pos: pos
    switch typeof link
      when "string" then params.link = link
      when "object"
        params.link = link.href
        params.label = link.text
    (cb) -> TrackedLink.track params, cb
  async.parallel requests, (err, results) ->
    done err if err?
    done null, results.map (result, index) ->
      resultLink = result.link
      return resultLink if /^(mailto|ftp|tel|telnet):/.test resultLink
      return resultLink if resultLink?.indexOf("mailrate.com/track") > -1
      unless /^https?:/.test resultLink
        resultLink = "http://#{result.link}"
      return "http://mailrate.com/track/#{result.shortId}/#{resultLink}"

module.exports = TrackedMessage = mongoose.model 'TrackedMessage', schema
