express = require 'express'
csv = require 'express-csv'
exphbs = require 'express-handlebars'
http = require 'http'
mongoose = require 'mongoose'
session = require 'express-session'
MongoStore = require('connect-mongo')(session)
routes = require './routes'

#passport = require 'passport'
#GooglePlusStrategy = require('passport-google-plus').Strategy

app = express()
port = process.env.PORT or 6666

# Set handlebars as the templating engine
app.engine 'handlebars', exphbs(defaultLayout: 'main')
app.set 'view engine', 'handlebars'

# Disable etag headers on responses to prevent browser caching
app.disable 'etag'

# Connect to MongoDB
mongoose.connect 'mongodb://localhost/mailrate', (err) ->
	console.log "Error connecting to Mongo: ", err if err?

# Sessions
app.use session
  resave: false
  saveUninitialized: false
  secret: "3fnl34ifghu3liugh"
  store: new MongoStore
    mongooseConnection: mongoose.connection

# Google Auth
# passport.use new GooglePlusStrategy
#   clientId: '1098310743763-1okt2a9nsmodh6na91cmbnaeeo5b6ddb.apps.googleusercontent.com'
#   clientSecret: 'O6H05SpCaAamYgEiGAMOIk90'
# , (identifier, profile, done) ->
#   console.log "PASSPORT LOGIN ***********"
#   console.log identifier
#   console.log profile
#   done null, profile, identifier

isAdmin = (req, res, next) ->
  console.log "Checking admin"
  unless req.session.adminUser?
    console.log "Trying to access admin route #{req.route.path}"
    req.session.on_auth_ok = req.route.path
    res.redirect '/login'
    return
  next()

##########
# Routes #
##########

# Home
app.get '/', routes.index

# Gmail Data Import
app.get '/import', routes.import

# Admin
app.get '/admin/links', isAdmin, routes.ltAdmin
app.get '/admin/logout', isAdmin, routes.adminLogout
app.get '/admin', isAdmin, routes.admin


# Extension API
app.get '/api/:method/:email?', routes.api

# Link tracking
app.get '/track/:trackingId/:protocol/:url*', routes.track

# User dashboard
app.get '/dashboard/:email?', routes.dashboard

# Click JSON for admin
app.get '/json/clicks/:email?', routes.clicks_json

# CSV export
app.get '/csv/:email/:dataset', routes.csv

# Auth
app.get '/auth', routes.auth
app.get '/login', routes.login
app.get '/logout', routes.logout

#app.post '/auth/google/callback', passport.authenticate 'google', (req, res) -> res.send req.user

# Set /public as static content dir
app.use '/', express.static(__dirname + '/public/')

# Jacked up and good to go
server = http.createServer(app).listen port, ->
	console.log 'Express server listening on port ' + port
