$(function(){

    function initBackgrid(){
        Backgrid.InputCellEditor.prototype.attributes.class = 'form-control input-sm';

        var Click = Backbone.Model.extend({});

        var PageableClicks = Backbone.PageableCollection.extend({
            model: Click,
            url: "/json/clicks/" + currentUser,
            state: {
                pageSize: 9
            },
            mode: "client" // page entirely on the client side
        });

	var HumanReadableDateCell = Backgrid.DatetimeCell.extend({
	    formatter: {
		fromRaw: function(rawData, model) {
		    parts = moment(rawData).format("MMM DD h:mm a").split(" ");
		    amPm = parts.pop().substr(0,1);
		    return parts.join(" ") + amPm;
		},
		toRaw: function(formattedData, model) {
		    return moment(formattedData);
		}
	    }
	});

        var pageableClicks = new PageableClicks(),
            initialClicks = pageableClicks;
        function createBackgrid(collection){
            var columns = [
		/*{
                name: "_id", // The key of the model attribute
                label: "ID", // The name to display in the header
                editable: false, // By default every cell in a column is editable, but *ID* shouldn't be
                // Defines a cell type, and ID is displayed as an integer without the ',' separating 1000s.
                cell: Backgrid.IntegerCell.extend({
                    orderSeparator: ''
                })
            },*/ {
                name: "date_sent",
                label: "Date Sent",
                cell: HumanReadableDateCell,
		editable: false
            }, {
                name: "subject",
                label: "Subject",
                cell: "string",
		editable: false
            }, {
                name: "link",
                label: "Link",
                cell: "uri",
		editable: false
            }, {
		name: "to",
		label: "Recipients",
		cell: Backgrid.Cell.extend({
		    className: "html-cell",
    
		    initialize: function () {
			Backgrid.Cell.prototype.initialize.apply(this, arguments);
		    },

		    render: function () {
			this.$el.empty();
			var rawValue = this.model.get(this.column.get("name"));
			var formattedValue = this.formatter.fromRaw(rawValue, this.model);
			this.$el.append(formattedValue);
			this.delegateEvents();
			return this;
		    }
		}),
		editable: false
	    }, {
                name: "clicked_moment",
                label: "Clicked At",
                cell: HumanReadableDateCell,
		direction: "descending",
		editable: false
            }, {
                name: "ip",
                label: "IP Address",
                cell: "string",
		editable: false
            }];
            if (LightBlue.isScreen('xs')){
                columns.splice(3,1)
            }
            var pageableGrid = new Backgrid.Grid({
                columns: columns,
                collection: collection,
                className: 'table table-striped table-editable no-margin mb-sm'
            });

            var paginator = new Backgrid.Extension.Paginator({

                slideScale: 0.25, // Default is 0.5

                // Whether sorting should go back to the first page
                goBackFirstOnSort: false, // Default is true

                collection: collection,

                controls: {
                    rewind: {
                        label: '<i class="fa fa-angle-double-left fa-lg"></i>',
                        title: "First"
                    },
                    back: {
                        label: '<i class="fa fa-angle-left fa-lg"></i>',
                        title: "Previous"
                    },
                    forward: {
                        label: '<i class="fa fa-angle-right fa-lg"></i>',
                        title: "Next"
                    },
                    fastForward: {
                        label: '<i class="fa fa-angle-double-right fa-lg"></i>',
                        title: "Last"
                    }
                }
            });

            $("#table-dynamic").html('').append(pageableGrid.render().$el).append(paginator.render().$el);
        }

        PjaxApp.onResize(function(){
            createBackgrid(pageableClicks);
        });

        createBackgrid(pageableClicks);

        $("#search-links").keyup(function(){
            var $that = $(this),
                filteredCollection = initialClicks.fullCollection.filter(function(el){
                    return ~el.get('link').toUpperCase().indexOf($that.val().toUpperCase());
                });
            createBackgrid(new PageableClicks(filteredCollection, {
                state: {
                    firstPage: 1,
                    currentPage: 1
                }
            }));
        });

        pageableClicks.fetch();
    }

    function pageLoad(){
        $('.widget').widgster();
        initBackgrid();
    }

    pageLoad();
    PjaxApp.onPageLoad(pageLoad);

});
