$( function() {
  authedUsers = {};
  getCurrentUser = function() {
      var pathParts = location.pathname.split("/");
      var user = pathParts.length < 3 ? null : pathParts[2];
      console.log("Current user", user);
      return user;
  };
  document.addEventListener("MailrateUsers", function(ev) {
      var authUsers = ev.detail.users, userOptions = "";
      console.log("[MailrateUsers caught]", authedUsers, authUsers);
      for (user in authUsers) {
	  if (user in authedUsers) continue;
	  userOptions += "<option value='" + user + "'>" + user + "</option>";
	  authedUsers[user] = true;
      }
      $("#login-select").append(userOptions);
      var currentUser = getCurrentUser();
      if (currentUser) $("#login-select").val(currentUser);
      console.log("Appended userOptions", userOptions);
  });
  $('#login-select').on('change', function() {
      var selectedUser = $(this).val();
      console.log(selectedUser);
      if (getCurrentUser() !== selectedUser) {
	  location.href = "/dashboard/" + selectedUser;
      }
  });
  if (window.chrome !== undefined) {
    chrome.webstore.onInstallStageChanged.addListener(
    	function(stage) {
    	    console.log("Install state changed", stage);
    	}
    );
    chrome.webstore.onDownloadProgress.addListener(
    	function(percent) {
    	    console.log("Downloading, %s percent complete", percent*100);
    	}
    );
  }
  $('a.step.two').on('click', function(e) {
    if (window.chrome !== undefined) {
      e.preventDefault();
      if ($(this).find(".current.version").is(":visible")) return;
      if ($(this).find(".label").is(":visible")) return;
      chrome.webstore.install(
	      "https://chrome.google.com/webstore/detail/nljjcfhhokboknahjknlhbjbggmlicji", 
        function() {
	        $(".version.none .button").replaceWith(
            "<div class='label'>Install Complete</div>");
        }, function(error, code) {
	        console.log("Error (%s) %s", code, error);
        }
      );
    }
  });
  currentVersion = $("#currentMailrateVersion").val();
  var outdated = function(installed, current) {
      vC = current.split('.');
      vI = installed.split('.');
      for (var i = 0; i < vC.length; i++) {
    	  if (i+1 > vI.length) return true;
    	  if (parseInt(vI[i]) < parseInt(vC[i])) return true;
      }
      return false;
  };
  document.addEventListener("MailrateVersion", function(version) {
      installedVersion = version.detail.version;
      if (outdated(installedVersion, currentVersion)) {
	      $(".version").hide();
        $(".version.old").show();
      } else {
	      $(".version").hide();
        $(".version.current").show();
      }
  }, false);
  var num = $("div.authMore").find("ul").children('li').length;
  if (num > 0) {
    //console.log("FOUND EMAIL ADDRESSES: " + num);
    $("div.noAuth").hide();
    $("div.authMore").show();
  }
});
