/**
 * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var readline = require('readline');
var google = require('googleapis');
var OAuth2Client = google.auth.OAuth2;
var gmail = google.gmail('v1');

// Client ID and client secret are available at
// https://code.google.com/apis/console
var CLIENT_ID = '40960681208-uacdv7sfvj1bu35v1kv4j16mie0jjvh6.apps.googleusercontent.com';
var CLIENT_SECRET = '4Z28IxRMeLo5OS4PSxreoqsZ';
var REDIRECT_URL = 'https://adum.io:8787/emailyze/oauth2callback';

var oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function getAccessToken(oauth2Client, callback) {
  // generate consent page url
  var url = oauth2Client.generateAuthUrl({
    access_type: 'offline', // will return a refresh token
    scope: 'https://www.googleapis.com/auth/gmail.readonly' // can be a space-delimited string or an array of scopes
  });

  console.log('Visit the url: ', url);
  rl.question('Enter the code here:', function(code) {
    // request access token
    oauth2Client.getToken(code, function(err, tokens) {
      // set tokens to the client
      // TODO: tokens should be set by OAuth2 client.
      oauth2Client.setCredentials(tokens);
      callback();
    });
  });
}

function handleMessages(oauth2Client, messages) {
    console.log(messages);
    firstEmail = messages.shift();
    console.log("ID of first email is " + firstEmail.id);
    fetchEmail(oauth2Client, firstEmail.id);
};


function fetchEmail(oauth2Client, messageId) {
    console.log("Fetching message " + messageId);
    gmail.users.messages.get({
	userId: 'me',
	id: messageId,
	auth: oauth2Client
    }, function(err, result) {
	if (err) {
	    console.log('An error occured', err);
	    return;
	}
	console.log(JSON.stringify(result, null, 4));
    });
}

// retrieve an access token
getAccessToken(oauth2Client, function() {
  // retrieve user profile
  gmail.users.messages.list({
      userId: 'me', 
      auth: oauth2Client
  }, function(err, results) {
      if (err) {
	  console.log('An error occured', err);
	  return;
      }
      console.log(results);
      if (results && results.messages) {
	  console.log("Fetched " + results.messages.length + " emails");
	  handleMessages(oauth2Client, results.messages);
      }
  });
});

