// NQB8 MailRate MVP Chrome Extension
// Background Script

serialize = function(obj) {
   var str = [];
   for(var p in obj){
       if (obj.hasOwnProperty(p)) {
           str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
       }
   }
   return str.join("&");
}

authStatus = {};

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    //console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
    console.log(request, sender);
    if (request.type == "api") {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "http://dev.mailrate.com/api/" + request.method + '/' + request.email + '?user=' + request.user, true);
      xhr.onreadystatechange = function() {
        if (xhr.readyState !== 4) return;
        var parsedResponse = JSON.parse(xhr.responseText);
        if (request.method === "auth") {
          if (parsedResponse && parsedResponse.status) {
            authStatus[parsedResponse.email] = true;
          }
        }
      	sendResponse(parsedResponse);
      };
      xhr.send();
      return true;	
  	} else if (typeof request == "string") {
      if (request == "checkAuth") {
        sendResponse(authStatus);
      } else if (request == "showAuth") {
        chrome.pageAction.show(sender.tab.id);
        sendResponse(null);
      } else if (request == "getVersion") {
        sendResponse(chrome.app.getDetails().version);
      }
    }
});