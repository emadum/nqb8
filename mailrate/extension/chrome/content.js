// NQB8 MailRate MVP Chrome Extension
// Content Script
!function() {
  var cachedStats = {};
	if (window.MailRateEnabled) {
		console.log("MailRate is already running.");
		return;
	}
	window.MailRateEnabled = true;
  if (window.location.hostname == "mail.google.com") {
  	var currentUser;
  	var loginChecker = setInterval(function() {
  		currentUser = getCurrentUser();
  		if (currentUser) {
  			clearInterval(loginChecker);
  			console.log("Current user detected: " + currentUser);
  			chrome.runtime.sendMessage({
  				type: "api",
  				method: "auth",
  				email: currentUser
  			}, function(authResponse) {
  				if (!authResponse.status) {
  					chrome.runtime.sendMessage("showAuth", null);
  				} else {
            initUI();
          }
  			});
  		}	
  	}, 100);
  } else if (window.location.hostname.indexOf("mailrate.com") !== -1) {
    chrome.runtime.sendMessage("getVersion", function(version){
      document.dispatchEvent(new CustomEvent("MailrateVersion", {
        detail: {version: version}
      }));
    });
    chrome.runtime.sendMessage("checkAuth", function(authUsers) {
      var added = $("#addedMailrateAuth"), waiting = false;
      if (added.length) {
        waiting = true;
        console.log("Checking auth on user => ", added.val());
        chrome.runtime.sendMessage({
          type: "api",
          method: "auth",
          email: added.val()
        }, function(authResponse) {
          if (authResponse.status) {
            authUsers[authResponse.email] = true;
          }
          showUsers(authUsers);
        });
      }
      if (!waiting) {
        showUsers(authUsers);
      }
    })
  } else {
    console.log("MailRate disabled for " + window.location.hostname);
  }

  var showUsers = function(authUsers) {
    if (!Object.keys(authUsers).length) return;
      $("div.noAuth").hide();
      var userList = $("div.authMore").show().find("ul");
      for (user in authUsers) {        
        userList.append("<li>" + user + "</li>");
      }
  }

	var getCurrentUser = function() {
		var email = $(".gb_Aa").text();
		if (email.indexOf('@') === -1) {
			email = $("a.gb_K.gb_ka.gb_n").text();
		}
		if (email.indexOf('@') === -1) {
			email = $("a.gb_K.gb_ka.gb_n").attr("title");
			if (matches = /\((.+)\)/.exec(email)) {
				email = matches[1];
			}
		}
		if (!email || email.indexOf('@') === -1) {
			console.log("Failed to determine current e-mail address");
		}
		return email;
	}

	var lookupStats = function(email, cb) {
		var currentUser =  getCurrentUser();
		console.log("Current user: " + currentUser);
		chrome.runtime.sendMessage({
	        type: "api",
	        method: "stats",
	        user: currentUser,
	       	email: email
	    }, function (response) {
        response.median_response_time = 
        response.median_response_time.replace("minute","min").replace("hour","hr").replace("an","1").replace("mins","min")
        cb(response);
      });
	}

	var parseContact = function(nameAndEmail) {
		matches = /^(.+) < *(.+) *> *$/.exec(nameAndEmail);
		if (matches == null) return {email: nameAndEmail};
		return {
			name: matches[1],
			email: matches[2]
		};
	};

	// Get the email address of the person in the current thread message
	var getEmail = function(cb) {
		window.setTimeout(function() {
			var $currentEmail = $("div.gs:not(.gt) span[email]");
			var $firstEmail = $("h3.iw span[email]");
			if ($currentEmail.length) {
				cb($currentEmail.attr("email"));
			/*} else if ($firstEmail.length) {
				console.log("Only one message in thread");
				cb($firstEmail.attr("email"));*/
			} else {
				//console.log("No current e-mail yet");
			}
		}, 600);
	}

	// Get the currently selected person in the sidebar
	var getCurrentPerson = function(parentEl, cb) {
		console.log("Finding person in container", parentEl);
		window.setTimeout(function() {
			var email = false;
			var $el1 = $("td.anO.anN>span", parentEl);
			var $el2 = $(".anR.Gr>img", parentEl);
			console.log($el1, $el2);
			if ($el1.length) {
				email = $el1.text();
			} else if ($el2.length) {
				email = $el2.attr("jid");
			} else {
				console.log("Email el not found");
			}
			cb(email);
		}, 200);
	}

  // Compose view functionality
  var addContact = function(email, where) {
    var contact = parseContact(email);
    console.log("Contact added => " + contact.email + " in " + where);

    if (!$("#mailrate-" + where).length)
      $("div.aoD.hl", parentEl[where]).after("<div id='mailrate-" + where+ "'></div>"); 

    lookupStats(contact.email, function(response) {
      var html = "<div class='contact' email='" + contact.email + "'>";
      responseRate = false;
      if (response.response_rate !== "NaN%" && response.response_rate !== "N/A") {
        html += contact.email + " responds to " + response.response_rate + " of emails. ";
        responseRate = true;
      }
      if (response.median_response_time !== "N/A") {
        html += (responseRate ? "Emails are answered within " : (contact.email + " responds within "))
         + response.median_response_time + " on average.";
      }
      
      html += "</div>";
      $("#mailrate-" + where).append(html);
    });
  };

  var addInlineKey = function(el) {
    var parentEl = "table.GS", where = "compose";
    if ($(el).parents('.IG').length) {
      parentEl = "table.IG", where = "reply";
    }
    console.log("Adding inline stats to " + where);
    console.log($(el).parents(parentEl).children("tbody"));
    if (!$(".mailrate-" + where).length) {      
      $(el).parents(parentEl).children("tbody").append(
        "<tr class='mailrate-" + where + "'></tr>");
      //if (!$(el).is(":visible")) $("tr.mailrate-" + where).hide();
      $("tr.mailrate-" + where).append(
        "<td colspan='2'>Email | <span class='hasTip'>" + 
        "Response Rate <sup class='rr-hover'>?</sup></span> |" + 
        "<span class='hasTip'> Response Time " + 
        "<sup class='rt-hover'>?</sup></span>" +
        "<div class='inline-brand'><em>Stats provided by</em> " + 
              "<img src='http://mailrate.com/mailrate.png'/> " + 
              "<span>ailRate</span></div>" +
            "</div></td>");
      $("tr.mailrate-" + where).find(".rr-hover").parent().append(
        getTooltipHtml("Response Rate", "The probability that a contact will respond.")       
      );
      $("tr.mailrate-" + where).find(".rt-hover").parent().append(
        getTooltipHtml("Response Time", "When a contact does respond, the time it takes them to respond.")       
      );
    } else {
      console.log("Key already found", $(".mailrate-" + where));
    }
  }

  var getTooltipHtml = function(title, body) {
    return '<div class="tip right">' + 
        '<div class="tipTitle">' + title + '</div>' + 
        '<div class="tipBody">' + body + '</div>' + 
        '<div class="tipArrow"></div></div>';
  };

  var addInlineStats = function(el) {
    addInlineKey(el);

    if ($(el).find("span.mr-inline-stats").length) return;
    var contact = parseContact($("input", el).val());
    // Add info
    if (cachedStats[contact.email]) {
      var html = getStatsEl(cachedStats[contact.email]);
      if (html) $(el).find("div.vT").append(html);
      return;
    }
    lookupStats(contact.email, function(response) {
      cachedStats[contact.email] = response;
      var html = getStatsEl(response);
      if (html) $(el).find("div.vT").append(html);
    });

  }

  var getStatsEl = function(stats) {
    var haveRR = stats.response_rate !== "NaN%" && stats.response_rate !== "N/A",
      haveRT = stats.median_response_time !== "N/A";
    html = "<span class='mr-inline-stats'>";
    if (haveRR) html += " | <span class='mr-inline-rr'>" + stats.response_rate + "</span>";
    if (haveRT) html += " | <span class='mr-inline-rt'>" + stats.median_response_time + "</span>";
    html += "</span>";
    return haveRR || haveRT ? html : null;
  }

  /*var removeContact = function(email, where) {
    var contact = parseContact(email);
    console.log("Contact removed => " + contact + " in " + where);
    mailId = "#mailrate-" + where;
    $(mailId).find("div.contact[email='" + contact.email + "']").remove();
    if (!$(mailId).children().length)
      $(mailId).remove();
  };*/

  var selectContact = function(email) {
    var contact = parseContact(contact);
    console.log("Contact selected: " + contact.email);
  };

  var widgetInterval;
  var refreshWidget = function(email) {
    widgetInterval = setInterval( function() {
      showWidget(email);
    }, 5000 );
  };

  // Sidebar widget
  var showWidget = function(email) {
    console.log("Running lookup on:" + email);
    lookupStats(email, function(response) {
      //console.log(response);
      //$sidebarEl.parent().parent().parent().parent().append(
      var responseTimeCard = 
          "<div class='widget'>Median response time" + 
              "<div class='value'>" + response.median_response_time + "</div>" +
              //"<a href='#'>More info</a>" + 
          "</div>";
      var responseRateCard = "<div class='widget'>Likelihood of response";
      if (response.processing) {
        responseRateCard += "<div class='value'><img class='loading' src='http://mailrate.com/loading.gif'/></div>";
        if (widgetInterval == null) refreshWidget(email);
      } else {
        responseRateCard += "<div class='value'>" + response.response_rate + "</div>";
        //"<a href='#'>More info</a>";
        if (widgetInterval != null) {
          clearInterval(widgetInterval);
          widgetInterval = null;
        }
      }
      responseRateCard += "</div>";
      widgetHtml = "<div id='mailrate'>" + 
        "<div style='text-align: center;font-weight:bold;color:white'>" +
        email + "</div>" + responseTimeCard + responseRateCard +
          "<div class='brand'><em>Stats provided by</em> " + 
          "<img src='http://mailrate.com/mailrate.png'/> " + 
          "<span>ailRate</span></div>" +
        "</div></div>";
      if ($("#mailrate").length) {
        $("#mailrate").replaceWith(widgetHtml);
      } else {
        $("div.u5").before(widgetHtml);
      }
    });
  }

  var initUI = function() {
  	// Monitor for scrolling ever 100ms and reposition the sidebar div if necessary
  	setInterval(function() {
  		if ($('div.ApVoH.s').length) return;
  		el = $("#mailrate");
  		if (!el.length) return;
  	    if (typeof jQuery === "function" && el instanceof jQuery) {
  	        el = el[0];
  	    }
  	    var rect = el.getBoundingClientRect();
  	    //console.log(rect.top >= 0);
  	    if (rect.top < 0) {
  	    /*	$(el).removeClass('sticky');
  	    } else if (!$(el).hasClass('sticky')) {*/
  	    	console.log($(el).width());
  	    	$(el).css("width", $(el).width()).addClass('sticky');
  	    } else if (!$("div.ar4").hasClass("aeI")) {
  	    	$(el).removeClass('sticky').css("width", "auto");
  	    }
  	}, 100);

  	// Switch between contacts in threads when e-mails are clicked
  	$("body").on("click", "table.cf.an6 tr", function() {
  		getCurrentPerson($("div.anr:visible")[0], function(email) {
  			showWidget(email);
  		});
  	});

  	// Handle switching of contacts in the right-hand sidebar
  	var threadRowSelector = "div.hn,div.h7,div.kn,div.kv";
  	$("body").on("mousedown", threadRowSelector, function() {
  		console.log($(this).attr("class"));
  		if ($(this).hasClass("kQ")) {
  			console.log($(this), "hasclass kQ!");
  			return;
  		}
  		showWidget($(this).find("span[email]").attr("email"));
  		$(threadRowSelector).css("border-right", "none");
  		$(this).css("border-right", "4px solid crimson");
  	});

  	// Inbox view functionality
  	var emailWatcher = window.setInterval( function() {
  		getEmail(function() {
  			clearInterval(emailWatcher);
  		});
  	}, 200 );

  	// Monitor switching between inbox and thread/email view
  	var threadViewObs = new MutationSummary({
  	    callback: function(summaries) {
  	        if (summaries[0].removed.length) {
  	            //console.log("IN INBOX");
  	        } else if (summaries[0].added.length) {
  	        	getEmail(function(email) {
  	        		console.log("Most recent email from " + email);
  	        		showWidget(email);
  	        	});
  	        }
  	    },
  	    queries: [{
  	        element: 'td.Bu.y3'
  	    }]
  	});
	
  	var composeContactObs = new MutationSummary({
  		callback: function(summaries) {
  			for (i in summaries[0].added) addInlineStats(summaries[0].added[i]);
  			/*if (summaries[0].removed.length) {
  				removeContact($("input", summaries[0].removed[0]).val(), 
  					$(summaries[0].getOldParentNode(summaries[0].removed[0])).parents('.IG').length ? "reply" : "compose");
  			} else {				
  				addContact($("input", summaries[0].added[0]).val(),
  					$(summaries[0].added[0]).parents('.IG').length ? "reply" : "compose");
  				$(summaries[0].added[0]).click(function() {
  					selectContact($(this).find("input").val());
  				});
  			}*/

  		},
  		queries: [{
  			element: 'div.vR'
  		}]
  	});
  }

}();

/*var composeWindowCollapsedObs = new MutationSummary({
	queries: [{
		element: 'div.AD'
	}]
});

var composeWindowExpandedObs = new MutationSummary({
	queries: [{
		element: 'div.aVN'
	}]
})
*/
